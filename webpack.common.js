const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const webpack = require('webpack');
const ENV = process.env.NODE_ENV || 'development';

module.exports = {
  entry: {
    index: './src/index.js'
  },
  plugins: [
    // new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      inject: true,
      template: 'template.html'
    }),
    new webpack.EnvironmentPlugin({
      NODE_ENV: ENV,
      DEBUG: false,
      TRACE_TURBOLINKS: ENV
    })
  ],
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.(sa|sc|c)ss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ['file-loader']
      },
      // {
      //   test: /\.js$/,
      //   use: [{ loader: 'babel-loader' }]
      // },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ['file-loader']
      }
    ]
  }
};
