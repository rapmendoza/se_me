# README

### 1. Clone repository

```git clone https://rapmendoza@bitbucket.org/rapmendoza/se_me.git```

### 2. NPM install

```npm install```

### 3. NPM start

```npm start```

### Who do I talk to?

Rap Mendoza

- [GitHub](http://github.com/rapmendoza)
- [BitBucket](http://bitbucket.org/rapmendoza)
