/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./dist/style.css":
/*!************************!*\
  !*** ./dist/style.css ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js!../node_modules/sass-loader/dist/cjs.js!./style.css */ "./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./dist/style.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);

var exported = content.locals ? content.locals : {};



module.exports = exported;

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./dist/style.css":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./dist/style.css ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "@font-face {\n  font-family: \"Raleway\";\n  font-style: normal;\n  font-weight: 400;\n  font-display: swap;\n  src: url(https://fonts.gstatic.com/s/raleway/v14/1Ptug8zYS_SKggPNyCMIT5lu.woff2) format(\"woff2\");\n}\n* {\n  font-family: \"Raleway\", sans-serif;\n  margin: 0;\n  padding: 0;\n}\n\nbody {\n  background-color: #f2f2f2;\n}\n\n.container {\n  margin: 0 auto;\n  padding: 1.25rem;\n}\n\n.container > * {\n  margin: 2rem 0;\n}\n\n.container:first-child {\n  margin-top: 0;\n}\n\n.container:last-child {\n  margin-bottom: 0;\n}\n\n.c-info__list {\n  padding: 6px 0;\n  font-size: small;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.c-col__7 .c-info__list:first-child {\n  font-weight: 700;\n  font-size: x-large;\n}\n\n.c-col__7 .c-info__list:nth-child(2), .c-col__7 .c-info__list:nth-child(3) {\n  -webkit-box-align: center;\n  -ms-flex-align: center;\n  align-items: center;\n}\n\n.c-col__7 .c-info__list > * {\n  padding-right: 8px;\n}\n\n.c-icon {\n  height: 1.3rem;\n  width: 1.3rem;\n}\n\n@media (max-width: 768px) {\n  .container {\n    width: 750px;\n  }\n}\n@media (min-width: 768px) {\n  .container {\n    width: 970px;\n  }\n}\n@media (min-width: 992px) {\n  .container {\n    width: 1170px;\n  }\n}\n.c-header {\n  background-color: #fff;\n  min-height: 240px;\n}\n\n.c-header .c-button__wrapper {\n  display: table-cell;\n  vertical-align: middle;\n}\n\n.c-header .c-button {\n  right: 15px;\n  float: right;\n  position: relative;\n  padding: 5px 12px;\n}\n\n.c-header .c-button :hover {\n  cursor: pointer;\n}\n\n@media (max-width: 768px) {\n  .container {\n    width: 540px;\n  }\n\n  .c-header .c-button {\n    border: 0;\n    background-color: #fff;\n    color: #1e88e5;\n    font-weight: 700;\n    text-transform: uppercase;\n  }\n\n  .c-header .c-layout:first-child {\n    min-height: 65px;\n    display: table;\n    width: 100%;\n  }\n\n  .c-header .c-layout:nth-child(2) {\n    display: none;\n  }\n\n  .c-header .c-layout:last-child {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 1fr [12];\n    grid-template-columns: repeat(12, 1fr);\n    min-height: 200px;\n  }\n\n  .c-header .c-container__25-d {\n    grid-column: span 12;\n    margin: auto;\n  }\n\n  .c-header .c-container__25-d .c-container {\n    width: 80%;\n    display: block;\n    margin-left: auto;\n    margin-right: auto;\n  }\n\n  .c-header .c-container__25-d img {\n    width: 100%;\n    position: relative;\n  }\n\n  .c-header .c-container__75-d {\n    grid-column: span 12;\n  }\n\n  .c-header .c-container__75-d .c-container {\n    padding-left: 15px;\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 1fr [12];\n    grid-template-columns: repeat(12, 1fr);\n    height: 65%;\n    padding-top: 25px;\n  }\n\n  .c-header .c-container__75-d .c-container .c-col__7 {\n    grid-column: span 12;\n  }\n\n  .c-header .c-container__75-d .c-container .c-col__7 .c-info__list:first-child {\n    -webkit-box-pack: center;\n    -ms-flex-pack: center;\n    justify-content: center;\n  }\n\n  .c-header .c-container__75-d .c-container .c-col__5 {\n    grid-column: span 12;\n  }\n\n  .c-header .c-container__75-d .c-container .c-col__5 .c-info__list {\n    text-align: center;\n  }\n\n  .c-header .c-container__75-d .c-container .c-col__5 .c-info__list:last-child {\n    padding-top: 12px;\n  }\n\n  .c-header .c-container__75-d > .c-info__list {\n    padding-left: 15px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -ms-flex-align: center;\n    align-items: center;\n  }\n\n  .c-header hr {\n    display: none;\n  }\n}\n@media (min-width: 768px) {\n  .container {\n    width: 720px;\n  }\n\n  .c-header .c-layout:first-child {\n    min-height: 65px;\n    display: table;\n    width: 100%;\n  }\n\n  .c-header .c-layout:nth-child(2) {\n    min-height: 50px;\n  }\n\n  .c-header .c-layout:last-child {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 1fr [12];\n    grid-template-columns: repeat(12, 1fr);\n    min-height: 200px;\n  }\n\n  .c-header .c-container__25-d {\n    grid-column: span 3;\n    margin: auto;\n  }\n\n  .c-header .c-container__25-d .c-container {\n    width: 80%;\n    display: block;\n    margin-left: auto;\n    margin-right: auto;\n  }\n\n  .c-header .c-container__25-d img {\n    width: 100%;\n    position: relative;\n  }\n\n  .c-header .c-container__75-d {\n    grid-column: span 9;\n  }\n\n  .c-header .c-container__75-d .c-container {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 1fr [12];\n    grid-template-columns: repeat(12, 1fr);\n    height: 65%;\n    padding-top: 25px;\n  }\n\n  .c-header .c-container__75-d .c-container .c-col__7 {\n    grid-column: span 7;\n  }\n\n  .c-header .c-container__75-d .c-container .c-col__7 .c-info__list:first-child {\n    -webkit-box-pack: left;\n    -ms-flex-pack: left;\n    justify-content: left;\n  }\n\n  .c-header .c-container__75-d .c-container .c-col__5 {\n    grid-column: span 5;\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 1fr [12];\n    grid-template-columns: repeat(12, 1fr);\n  }\n\n  .c-header .c-container__75-d .c-container .c-col__5 .c-info__list {\n    grid-column: span 6;\n    text-align: center;\n  }\n\n  .c-header .c-container__75-d .c-container .c-col__5 .c-info__list:last-child {\n    padding-top: 12px;\n  }\n\n  .c-header .c-container__75-d > .c-info__list {\n    padding-left: 15px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -ms-flex-align: center;\n    align-items: center;\n    float: right;\n    right: 20px;\n    position: relative;\n  }\n\n  .c-header hr {\n    position: relative;\n    top: 150px;\n    z-index: auto;\n    background-color: #000;\n    display: block;\n    height: 1.5px;\n  }\n}\n.c-nav {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -ms-flex-align: center;\n  align-items: center;\n  -webkit-box-pack: justify;\n  -ms-flex-pack: justify;\n  justify-content: space-between;\n}\n\n.c-nav ul {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  list-style-type: none;\n}\n\n.c-nav li {\n  padding: 10px;\n}\n\n.c-nav li:hover {\n  cursor: pointer;\n}\n\n.c-nav .active:after {\n  content: \"\";\n  display: block;\n  margin: 0 auto;\n  padding-top: 15px;\n  border-bottom: 2px solid black;\n}\n\n@media (max-width: 768px) {\n  .c-nav {\n    border-top: 2px solid #000;\n    padding-top: 5px;\n    margin-top: 10px;\n  }\n\n  .c-nav li {\n    padding-bottom: 0;\n  }\n}\n@media (min-width: 768px) {\n  .c-nav {\n    border: 0;\n    padding-top: 0;\n    margin-top: 0;\n  }\n\n  .c-nav li {\n    padding-bottom: 0;\n  }\n}\n.c-main {\n  background-color: #fff;\n}\n\n.c-item > * {\n  display: inline-block;\n  padding-right: 8px;\n}\n\n@media (max-width: 768px) {\n  .container {\n    width: 540px;\n  }\n\n  .c-main {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 1fr [12];\n    grid-template-columns: repeat(12, 1fr);\n    padding: 25px 20px;\n  }\n\n  .c-main .c-layout:first-child {\n    grid-column: span 12;\n    padding-bottom: 20px;\n  }\n\n  .c-main .c-layout h3 {\n    display: inline-block;\n  }\n\n  .c-main .c-layout button {\n    float: right;\n    padding: 5px 8px;\n  }\n\n  .c-main .c-layout .c-btn__cancel, .c-main .c-layout .c-btn__save {\n    display: none;\n    border: 0;\n    background-color: #fff;\n    color: #1e88e5;\n    font-weight: 700;\n    padding: 10px;\n    margin-right: 10px;\n  }\n\n  .c-main .c-layout .c-btn__cancel :hover, .c-main .c-layout .c-btn__save :hover {\n    cursor: pointer;\n  }\n\n  .c-main .c-layout:nth-child(2), .c-main form {\n    grid-column: span 6;\n  }\n\n  .c-main .c-item {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -ms-flex-align: center;\n    align-items: center;\n    padding: 5px 0;\n    font-size: small;\n  }\n\n  .c-main .c-item .c-edit {\n    display: none;\n  }\n\n  .c-main .c-item:first-child {\n    font-size: medium;\n    font-weight: 700;\n  }\n\n  .c-alter__form {\n    display: none;\n  }\n}\n@media (min-width: 768px) {\n  .container {\n    width: 720px;\n  }\n\n  .c-main {\n    display: -ms-grid;\n    display: grid;\n    -ms-grid-columns: 1fr [12];\n    grid-template-columns: repeat(12, 1fr);\n    padding: 25px 20px;\n  }\n\n  .c-main .c-layout:first-child {\n    grid-column: span 12;\n    padding-bottom: 20px;\n  }\n\n  .c-main .c-layout:first-child button {\n    display: none;\n  }\n\n  .c-main .c-layout:nth-child(2), .c-main form {\n    grid-column: span 6;\n  }\n\n  .c-main .c-item {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n    -ms-flex-align: center;\n    align-items: center;\n    padding: 5px 0;\n    font-size: small;\n  }\n\n  .c-main .c-item .c-edit {\n    display: block;\n  }\n\n  .c-main .c-item:first-child {\n    font-size: medium;\n    font-weight: 700;\n  }\n}\n.c-input--is-active {\n  border-bottom: 2px solid #1e88e5;\n  -webkit-transition: all 0.4s;\n  transition: all 0.4s;\n}\n\n.c-input--is-active .c-input__label {\n  color: #1e88e5;\n  font-size: 12px;\n  -webkit-transform: translateY(-14px);\n  transform: translateY(-14px);\n}\n\n.c-input--is-filled .c-input__label {\n  font-size: 12px;\n  -webkit-transform: translateY(-14px);\n  transform: translateY(-14px);\n}\n\n.c-input__label {\n  display: block;\n  font-size: 16px;\n  font-weight: normal;\n  color: #ccc;\n  left: 0;\n  margin: 0;\n  padding: 18px 12px 0;\n  position: absolute;\n  top: 0;\n  -webkit-transition: all 0.4s;\n  transition: all 0.4s;\n  width: 100%;\n}\n\n.c-input__group {\n  border-radius: 8px 8px 0 0;\n  overflow: hidden;\n  position: relative;\n  width: 100%;\n}\n\n.c-input__group::after {\n  border-bottom: 2px solid #1e88e5;\n  bottom: 0;\n  content: \"\";\n  display: block;\n  left: 0;\n  margin: 0 auto;\n  position: absolute;\n  right: 0;\n  -webkit-transform: scaleX(0);\n  transform: scaleX(0);\n  -webkit-transition: all 0.4s;\n  transition: all 0.4s;\n  width: 1%;\n}\n\n.c-input__field {\n  -webkit-appearance: none;\n  -moz-appearance: none;\n  appearance: none;\n  background: transparent;\n  border: 0;\n  border-bottom: 1px solid #999;\n  color: #333;\n  display: block;\n  font-size: 16px;\n  margin-top: 10px;\n  outline: 0;\n  padding: 14px 12px 10px 12px;\n  width: 100%;\n}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join('');
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === 'string') {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring

  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot || '').concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
  return "/*# ".concat(data, " */");
}

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isOldIE = function isOldIE() {
  var memo;
  return function memorize() {
    if (typeof memo === 'undefined') {
      // Test for IE <= 9 as proposed by Browserhacks
      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
      // Tests for existence of standard globals is to allow style-loader
      // to operate correctly into non-standard environments
      // @see https://github.com/webpack-contrib/style-loader/issues/177
      memo = Boolean(window && document && document.all && !window.atob);
    }

    return memo;
  };
}();

var getTarget = function getTarget() {
  var memo = {};
  return function memorize(target) {
    if (typeof memo[target] === 'undefined') {
      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
        try {
          // This will throw an exception if access to iframe is blocked
          // due to cross-origin restrictions
          styleTarget = styleTarget.contentDocument.head;
        } catch (e) {
          // istanbul ignore next
          styleTarget = null;
        }
      }

      memo[target] = styleTarget;
    }

    return memo[target];
  };
}();

var stylesInDom = [];

function getIndexByIdentifier(identifier) {
  var result = -1;

  for (var i = 0; i < stylesInDom.length; i++) {
    if (stylesInDom[i].identifier === identifier) {
      result = i;
      break;
    }
  }

  return result;
}

function modulesToDom(list, options) {
  var idCountMap = {};
  var identifiers = [];

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = options.base ? item[0] + options.base : item[0];
    var count = idCountMap[id] || 0;
    var identifier = "".concat(id, " ").concat(count);
    idCountMap[id] = count + 1;
    var index = getIndexByIdentifier(identifier);
    var obj = {
      css: item[1],
      media: item[2],
      sourceMap: item[3]
    };

    if (index !== -1) {
      stylesInDom[index].references++;
      stylesInDom[index].updater(obj);
    } else {
      stylesInDom.push({
        identifier: identifier,
        updater: addStyle(obj, options),
        references: 1
      });
    }

    identifiers.push(identifier);
  }

  return identifiers;
}

function insertStyleElement(options) {
  var style = document.createElement('style');
  var attributes = options.attributes || {};

  if (typeof attributes.nonce === 'undefined') {
    var nonce =  true ? __webpack_require__.nc : undefined;

    if (nonce) {
      attributes.nonce = nonce;
    }
  }

  Object.keys(attributes).forEach(function (key) {
    style.setAttribute(key, attributes[key]);
  });

  if (typeof options.insert === 'function') {
    options.insert(style);
  } else {
    var target = getTarget(options.insert || 'head');

    if (!target) {
      throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
    }

    target.appendChild(style);
  }

  return style;
}

function removeStyleElement(style) {
  // istanbul ignore if
  if (style.parentNode === null) {
    return false;
  }

  style.parentNode.removeChild(style);
}
/* istanbul ignore next  */


var replaceText = function replaceText() {
  var textStore = [];
  return function replace(index, replacement) {
    textStore[index] = replacement;
    return textStore.filter(Boolean).join('\n');
  };
}();

function applyToSingletonTag(style, index, remove, obj) {
  var css = remove ? '' : obj.media ? "@media ".concat(obj.media, " {").concat(obj.css, "}") : obj.css; // For old IE

  /* istanbul ignore if  */

  if (style.styleSheet) {
    style.styleSheet.cssText = replaceText(index, css);
  } else {
    var cssNode = document.createTextNode(css);
    var childNodes = style.childNodes;

    if (childNodes[index]) {
      style.removeChild(childNodes[index]);
    }

    if (childNodes.length) {
      style.insertBefore(cssNode, childNodes[index]);
    } else {
      style.appendChild(cssNode);
    }
  }
}

function applyToTag(style, options, obj) {
  var css = obj.css;
  var media = obj.media;
  var sourceMap = obj.sourceMap;

  if (media) {
    style.setAttribute('media', media);
  } else {
    style.removeAttribute('media');
  }

  if (sourceMap && btoa) {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    while (style.firstChild) {
      style.removeChild(style.firstChild);
    }

    style.appendChild(document.createTextNode(css));
  }
}

var singleton = null;
var singletonCounter = 0;

function addStyle(obj, options) {
  var style;
  var update;
  var remove;

  if (options.singleton) {
    var styleIndex = singletonCounter++;
    style = singleton || (singleton = insertStyleElement(options));
    update = applyToSingletonTag.bind(null, style, styleIndex, false);
    remove = applyToSingletonTag.bind(null, style, styleIndex, true);
  } else {
    style = insertStyleElement(options);
    update = applyToTag.bind(null, style, options);

    remove = function remove() {
      removeStyleElement(style);
    };
  }

  update(obj);
  return function updateStyle(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {
        return;
      }

      update(obj = newObj);
    } else {
      remove();
    }
  };
}

module.exports = function (list, options) {
  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
  // tags it will allow on a page

  if (!options.singleton && typeof options.singleton !== 'boolean') {
    options.singleton = isOldIE();
  }

  list = list || [];
  var lastIdentifiers = modulesToDom(list, options);
  return function update(newList) {
    newList = newList || [];

    if (Object.prototype.toString.call(newList) !== '[object Array]') {
      return;
    }

    for (var i = 0; i < lastIdentifiers.length; i++) {
      var identifier = lastIdentifiers[i];
      var index = getIndexByIdentifier(identifier);
      stylesInDom[index].references--;
    }

    var newLastIdentifiers = modulesToDom(newList, options);

    for (var _i = 0; _i < lastIdentifiers.length; _i++) {
      var _identifier = lastIdentifiers[_i];

      var _index = getIndexByIdentifier(_identifier);

      if (stylesInDom[_index].references === 0) {
        stylesInDom[_index].updater();

        stylesInDom.splice(_index, 1);
      }
    }

    lastIdentifiers = newLastIdentifiers;
  };
};

/***/ }),

/***/ "./src/assets/add-circle.svg":
/*!***********************************!*\
  !*** ./src/assets/add-circle.svg ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "199b115bd6278a9509590182291dade5.svg");

/***/ }),

/***/ "./src/assets/call-outline.svg":
/*!*************************************!*\
  !*** ./src/assets/call-outline.svg ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "2d7aeb424f4197447f9dc3c076785333.svg");

/***/ }),

/***/ "./src/assets/globe-outline.svg":
/*!**************************************!*\
  !*** ./src/assets/globe-outline.svg ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "f22e3b31c1d74331994b8d2940479ed3.svg");

/***/ }),

/***/ "./src/assets/home-outline.svg":
/*!*************************************!*\
  !*** ./src/assets/home-outline.svg ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "5d4dea4af09e2dba0cd0b1cb276d76e0.svg");

/***/ }),

/***/ "./src/assets/location-outline.svg":
/*!*****************************************!*\
  !*** ./src/assets/location-outline.svg ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "ca89f31a75323186cd6020a0bcdd9ba2.svg");

/***/ }),

/***/ "./src/assets/pencil-outline.svg":
/*!***************************************!*\
  !*** ./src/assets/pencil-outline.svg ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "769e9de606aee5d73573c57fcb5cd7cb.svg");

/***/ }),

/***/ "./src/assets/star-outline.svg":
/*!*************************************!*\
  !*** ./src/assets/star-outline.svg ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "19e0ca74b3a670aae254b6df17992246.svg");

/***/ }),

/***/ "./src/assets/star.svg":
/*!*****************************!*\
  !*** ./src/assets/star.svg ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "5b93c60e7b4142f41dfd8e95f05bdd4b.svg");

/***/ }),

/***/ "./src/components/header/index.js":
/*!****************************************!*\
  !*** ./src/components/header/index.js ***!
  \****************************************/
/*! exports provided: getComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getComponent", function() { return getComponent; });
/* harmony import */ var _profile_image_jpg__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./profile_image.jpg */ "./src/components/header/profile_image.jpg");
/* harmony import */ var _assets_star_outline_svg__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../assets/star-outline.svg */ "./src/assets/star-outline.svg");
/* harmony import */ var _assets_star_svg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../assets/star.svg */ "./src/assets/star.svg");
/* harmony import */ var _assets_add_circle_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../assets/add-circle.svg */ "./src/assets/add-circle.svg");
/* harmony import */ var _assets_call_outline_svg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../assets/call-outline.svg */ "./src/assets/call-outline.svg");
/* harmony import */ var _assets_location_outline_svg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../assets/location-outline.svg */ "./src/assets/location-outline.svg");
/* harmony import */ var _menu_index_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./menu/index.js */ "./src/components/header/menu/index.js");









function getComponent() {
  // create header
  let header = createElement('header', 'c-header');

  // create layout_1
  let layout_1 = createElement('div', 'c-layout');

  // create layout_2
  let layout_2 = createElement('div', 'c-layout');

  // create layout_3
  let layout_3 = createElement('div', 'c-layout');

  let btn_wrapper = createElement('div', 'c-button__wrapper');

  //create button
  let button = createElement('button', ['c-button', 'c-button--primary']);
  button.innerHTML = 'Logout';

  // create 25% desktop
  let d_25 = createElement('div', 'c-container__25-d');
  let d_25_con = createElement('div', 'c-container');

  // create 75% desktop
  let d_75 = createElement('div', 'c-container__75-d');
  let d_75_con = createElement('div', 'c-container');

  // create image
  let image = createElement('img');
  image.src = _profile_image_jpg__WEBPACK_IMPORTED_MODULE_0__["default"];

  // create infos
  let info_1 = createElement('div', 'c-info__list');
  info_1.innerHTML = 'Jessica Parker';

  let info_2 = createElement('div', 'c-info__list');
  let info_3 = createElement('div', 'c-info__list');

  let info_4 = createElement('div', 'c-info__list');

  let info_5 = createElement('div', 'c-info__list');
  info_5.innerHTML = '6 Reviews';

  // summon icons
  // star
  let icon_s_1 = createElement('img', 'c-icon');
  icon_s_1.src = _assets_star_svg__WEBPACK_IMPORTED_MODULE_2__["default"];
  let icon_s_2 = createElement('img', 'c-icon');
  icon_s_2.src = _assets_star_svg__WEBPACK_IMPORTED_MODULE_2__["default"];
  let icon_s_3 = createElement('img', 'c-icon');
  icon_s_3.src = _assets_star_svg__WEBPACK_IMPORTED_MODULE_2__["default"];
  let icon_s_4 = createElement('img', 'c-icon');
  icon_s_4.src = _assets_star_svg__WEBPACK_IMPORTED_MODULE_2__["default"];
  // star-outline
  let icon_so = createElement('img', 'c-icon');
  icon_so.src = _assets_star_outline_svg__WEBPACK_IMPORTED_MODULE_1__["default"];
  // add-circle
  let icon_a = createElement('img', 'c-icon');
  icon_a.src = _assets_add_circle_svg__WEBPACK_IMPORTED_MODULE_3__["default"];
  // location
  let icon_l = createElement('img', 'c-icon');
  icon_l.src = _assets_location_outline_svg__WEBPACK_IMPORTED_MODULE_5__["default"];
  // call
  let icon_p = createElement('img', 'c-icon');
  icon_p.src = _assets_call_outline_svg__WEBPACK_IMPORTED_MODULE_4__["default"];

  let info_6 = createElement('div', 'c-info__list');

  // create divider
  let col__8 = createElement('div', 'c-col__7');
  let col__4 = createElement('div', 'c-col__5');

  // create hr
  let hr = createElement('hr');

  // APPENDS
  // append icons
  info_4.appendChild(icon_s_1);
  info_4.appendChild(icon_s_2);
  info_4.appendChild(icon_s_3);
  info_4.appendChild(icon_s_4);
  info_4.appendChild(icon_so);
  info_2.appendChild(icon_l);
  info_3.appendChild(icon_p);

  // append button
  btn_wrapper.appendChild(button);
  layout_1.appendChild(btn_wrapper);

  // append image
  d_25_con.appendChild(image);
  d_25.appendChild(d_25_con);

  // append info
  info_2.innerHTML += 'Newport Beach, CA';
  info_3.innerHTML += '(949) 325-68594';

  col__8.appendChild(info_1);
  col__8.appendChild(info_2);
  col__8.appendChild(info_3);
  d_75_con.appendChild(col__8);

  col__4.appendChild(info_4);
  col__4.appendChild(info_5);
  d_75_con.appendChild(col__4);

  // append 75 to container
  d_75.appendChild(d_75_con);

  // append followers to container
  info_6.appendChild(icon_a);
  info_6.innerHTML += '15 Followers';
  d_75.appendChild(info_6);

  // append nav
  d_75.appendChild(Object(_menu_index_js__WEBPACK_IMPORTED_MODULE_6__["getComponent"])());

  // append containers
  layout_3.appendChild(d_25);
  layout_3.appendChild(d_75);

  // append layout-1
  header.appendChild(layout_1);

  // append layout-2
  header.appendChild(layout_2);

  // append hr
  header.appendChild(hr);

  // append layout-3
  header.appendChild(layout_3);

  return header;
}

function createElement(elTag, elClass) {
  let el = document.createElement(elTag);

  if (typeof elClass === 'string') {
    el.classList.add(elClass);
  } else if (typeof elClass === 'object') {
    if (elClass.length >= 1) {
      elClass.forEach(c => {
        el.classList.add(c);
      });
    }
  }

  return el;
}




/***/ }),

/***/ "./src/components/header/menu/index.js":
/*!*********************************************!*\
  !*** ./src/components/header/menu/index.js ***!
  \*********************************************/
/*! exports provided: getComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getComponent", function() { return getComponent; });
function getComponent() {
  // create nav
  let nav = createElement('nav', 'c-nav');

  // create ul
  let ul = createElement('ul', 'c-ul');

  // create list
  let li_1 = createElement('li', ['c-list', 'active']);
  let li_2 = createElement('li', 'c-list');
  let li_3 = createElement('li', 'c-list');
  let li_4 = createElement('li', 'c-list');
  let li_5 = createElement('li', 'c-list');

  // create link
  let a_1 = createElement('a', 'c-link');
  a_1.innerHTML = 'About';

  let a_2 = createElement('a', 'c-link');
  a_2.innerHTML = 'Settings';

  let a_3 = createElement('a', 'c-link');
  a_3.innerHTML = 'Option1';

  let a_4 = createElement('a', 'c-link');
  a_4.innerHTML = 'Option2';

  let a_5 = createElement('a', 'c-link');
  a_5.innerHTML = 'Option3';

  // APPENDS
  li_1.appendChild(a_1);
  li_2.appendChild(a_2);
  li_3.appendChild(a_3);
  li_4.appendChild(a_4);
  li_5.appendChild(a_5);

  ul.appendChild(li_1);
  ul.appendChild(li_2);
  ul.appendChild(li_3);
  ul.appendChild(li_4);
  ul.appendChild(li_5);

  nav.appendChild(ul);

  return nav;
}

function createElement(elTag, elClass) {
  // create element
  let el = document.createElement(elTag);

  // adds class
  if (typeof elClass === 'string') {
    el.classList.add(elClass);
  } else if (typeof elClass === 'object') {
    if (elClass.length >= 1) {
      elClass.forEach(c => {
        el.classList.add(c);
      });
    }
  }

  return el;
}

// const _getComponent = getComponent;



/***/ }),

/***/ "./src/components/header/profile_image.jpg":
/*!*************************************************!*\
  !*** ./src/components/header/profile_image.jpg ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "04034cc52e0c784e318ccc69d87273aa.jpg");

/***/ }),

/***/ "./src/components/main/form/group/index.js":
/*!*************************************************!*\
  !*** ./src/components/main/form/group/index.js ***!
  \*************************************************/
/*! exports provided: getFirstName, getLastName, getLocation, getPhone, getWebsite */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFirstName", function() { return getFirstName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLastName", function() { return getLastName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLocation", function() { return getLocation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPhone", function() { return getPhone; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getWebsite", function() { return getWebsite; });
function getComponent(name, value) {
  let group = createElement('div', ['c-input__group', 'c-input--is-filled']);

  let label = createElement('label', 'c-input__label');
  label.setAttribute('for', name);
  label.innerHTML = labelize(name);

  let field = createElement('input', 'c-input__field');
  field.setAttribute('type', 'text');
  field.setAttribute('name', name);
  field.setAttribute('value', value);

  // APPEND
  group.appendChild(label);
  group.appendChild(field);

  return group;
}

function createElement(elTag, elClass) {
  // create element
  let el = document.createElement(elTag);

  // adds class
  if (typeof elClass === 'string') {
    el.classList.add(elClass);
  } else if (typeof elClass === 'object') {
    if (elClass.length >= 1) {
      elClass.forEach(c => {
        el.classList.add(c);
      });
    }
  }

  return el;
}

function getFirstName() {
  return getComponent('first_name', 'Jessica');
}

function getLastName() {
  return getComponent('last_name', 'Parker');
}

function getLocation() {
  return getComponent('location', 'Newport Beach, CA');
}

function getPhone() {
  return getComponent('phone', '(949) 325-68594');
}

function getWebsite() {
  return getComponent('website', 'www.seller.com');
}

function labelize(name) {
  return name.replace('_', ' ').toUpperCase();
}




/***/ }),

/***/ "./src/components/main/form/index.js":
/*!*******************************************!*\
  !*** ./src/components/main/form/index.js ***!
  \*******************************************/
/*! exports provided: getComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getComponent", function() { return getComponent; });
/* harmony import */ var _group_index_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./group/index.js */ "./src/components/main/form/group/index.js");


function getComponent() {
  let form = createElement('form', ['c-form', 'c-alter__form']);

  form.appendChild(Object(_group_index_js__WEBPACK_IMPORTED_MODULE_0__["getFirstName"])());
  form.appendChild(Object(_group_index_js__WEBPACK_IMPORTED_MODULE_0__["getLastName"])());
  form.appendChild(Object(_group_index_js__WEBPACK_IMPORTED_MODULE_0__["getLocation"])());
  form.appendChild(Object(_group_index_js__WEBPACK_IMPORTED_MODULE_0__["getPhone"])());
  form.appendChild(Object(_group_index_js__WEBPACK_IMPORTED_MODULE_0__["getWebsite"])());

  return form;
}

function createElement(elTag, elClass) {
  // create element
  let el = document.createElement(elTag);

  // adds class
  if (typeof elClass === 'string') {
    el.classList.add(elClass);
  } else if (typeof elClass === 'object') {
    if (elClass.length >= 1) {
      elClass.forEach(c => {
        el.classList.add(c);
      });
    }
  }

  return el;
}




/***/ }),

/***/ "./src/components/main/index.js":
/*!**************************************!*\
  !*** ./src/components/main/index.js ***!
  \**************************************/
/*! exports provided: getComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getComponent", function() { return getComponent; });
/* harmony import */ var _assets_globe_outline_svg__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../assets/globe-outline.svg */ "./src/assets/globe-outline.svg");
/* harmony import */ var _assets_call_outline_svg__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../assets/call-outline.svg */ "./src/assets/call-outline.svg");
/* harmony import */ var _assets_home_outline_svg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../assets/home-outline.svg */ "./src/assets/home-outline.svg");
/* harmony import */ var _assets_pencil_outline_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../assets/pencil-outline.svg */ "./src/assets/pencil-outline.svg");
/* harmony import */ var _form_index_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./form/index.js */ "./src/components/main/form/index.js");






function getComponent() {
  // create main
  let main = createElement('main', 'c-main');

  // create layouts
  let layout_1 = createElement('div', 'c-layout');
  let layout_2 = createElement('div', ['c-layout', 'c-alter__display']);

  // create h3
  let h3 = createElement('h3');
  h3.innerHTML = 'About';

  // create button
  let button = createElement('button', 'c-btn__edit');
  let btn_cancel = createElement('button', 'c-btn__cancel');
  btn_cancel.innerHTML = 'CANCEL';
  let btn_save = createElement('button', 'c-btn__save');
  btn_save.innerHTML = 'SAVE';

  // create items
  let item_1 = createElement('div', 'c-item');
  let item_2 = createElement('div', 'c-item');
  let item_3 = createElement('div', 'c-item');
  let item_4 = createElement('div', 'c-item');

  let item_1_span = createElement('span');
  let item_2_span = createElement('span');
  let item_3_span = createElement('span');
  let item_4_span = createElement('span');

  item_1_span.innerHTML = 'Jessica Parker';
  item_2_span.innerHTML = 'www.seller.com';
  item_3_span.innerHTML = '(949) 325 - 68594';
  item_4_span.innerHTML = 'Newport Beach, CA';

  let item_1_container = createElement('div', 'c-container');
  let item_2_container = createElement('div', 'c-container');
  let item_3_container = createElement('div', 'c-container');
  let item_4_container = createElement('div', 'c-container');

  let item_1_edit = createElement('div', 'c-edit');
  let item_2_edit = createElement('div', 'c-edit');
  let item_3_edit = createElement('div', 'c-edit');
  let item_4_edit = createElement('div', 'c-edit');

  // icons
  let iconGlobe = createElement('img', 'c-icon');
  iconGlobe.src = _assets_globe_outline_svg__WEBPACK_IMPORTED_MODULE_0__["default"];

  let iconCall = createElement('img', 'c-icon');
  iconCall.src = _assets_call_outline_svg__WEBPACK_IMPORTED_MODULE_1__["default"];

  let iconHome = createElement('img', 'c-icon');
  iconHome.src = _assets_home_outline_svg__WEBPACK_IMPORTED_MODULE_2__["default"];

  let iconPencil_1 = createElement('img', 'c-icon');
  iconPencil_1.src = _assets_pencil_outline_svg__WEBPACK_IMPORTED_MODULE_3__["default"];

  let iconPencil_2 = createElement('img', 'c-icon');
  iconPencil_2.src = _assets_pencil_outline_svg__WEBPACK_IMPORTED_MODULE_3__["default"];

  let iconPencil_3 = createElement('img', 'c-icon');
  iconPencil_3.src = _assets_pencil_outline_svg__WEBPACK_IMPORTED_MODULE_3__["default"];

  let iconPencil_4 = createElement('img', 'c-icon');
  iconPencil_4.src = _assets_pencil_outline_svg__WEBPACK_IMPORTED_MODULE_3__["default"];

  let iconPencil_5 = createElement('img', 'c-icon');
  iconPencil_5.src = _assets_pencil_outline_svg__WEBPACK_IMPORTED_MODULE_3__["default"];

  // APPENDS
  button.appendChild(iconPencil_5);

  layout_1.appendChild(h3);
  layout_1.appendChild(button);
  layout_1.appendChild(btn_save);
  layout_1.appendChild(btn_cancel);

  item_1_edit.appendChild(iconPencil_1);
  item_2_edit.appendChild(iconPencil_2);
  item_3_edit.appendChild(iconPencil_3);
  item_4_edit.appendChild(iconPencil_4);

  item_1_container.appendChild(item_1_span);
  item_1.appendChild(item_1_container);
  item_1.appendChild(item_1_edit);

  item_2_container.appendChild(item_2_span);
  item_2.appendChild(iconGlobe);
  item_2.appendChild(item_2_container);
  item_2.appendChild(item_2_edit);

  item_3_container.appendChild(item_3_span);
  item_3.appendChild(iconCall);
  item_3.appendChild(item_3_container);
  item_3.appendChild(item_3_edit);

  item_4_container.appendChild(item_4_span);
  item_4.appendChild(iconHome);
  item_4.appendChild(item_4_container);
  item_4.appendChild(item_4_edit);

  layout_2.appendChild(item_1);
  layout_2.appendChild(item_2);
  layout_2.appendChild(item_3);
  layout_2.appendChild(item_4);

  main.appendChild(layout_1);
  main.appendChild(layout_2);

  // append nav
  main.appendChild(Object(_form_index_js__WEBPACK_IMPORTED_MODULE_4__["getComponent"])());

  return main;
}

function createElement(elTag, elClass) {
  let el = document.createElement(elTag);

  if (typeof elClass === 'string') {
    el.classList.add(elClass);
  } else if (typeof elClass === 'object') {
    if (elClass.length >= 1) {
      elClass.forEach(c => {
        el.classList.add(c);
      });
    }
  }

  return el;
}




/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_header_index_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/header/index.js */ "./src/components/header/index.js");
/* harmony import */ var _components_main_index_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/main/index.js */ "./src/components/main/index.js");


__webpack_require__(/*! ../dist/style.css */ "./dist/style.css");

// create container
let container = document.createElement('div');
container.classList.add('container');

// append container
document.body.appendChild(container);

// append header
container.appendChild(Object(_components_header_index_js__WEBPACK_IMPORTED_MODULE_0__["getComponent"])());

// append main
container.appendChild(Object(_components_main_index_js__WEBPACK_IMPORTED_MODULE_1__["getComponent"])());

// after append listeners
const setActive = (el, active) => {
  const formField = el.parentNode;
  if (active) {
    formField.classList.add('c-input--is-active');
  } else {
    formField.classList.remove('c-input--is-active');
    el.value === ''
      ? formField.classList.remove('c-input--is-filled')
      : formField.classList.add('c-input--is-filled');
  }
};

[].forEach.call(document.querySelectorAll('.c-input__field'), el => {
  el.onblur = () => {
    setActive(el, false);
  };

  el.onfocus = () => {
    setActive(el, true);
  };
});

[].forEach.call(document.querySelectorAll('.c-input__label'), el => {
  el.onclick = () => {
    setActive(el, true);
    el.nextSibling.focus();
  };
});

// show edit
const btn_e = document.querySelector('.c-btn__edit');
btn_e.onclick = () => {
  showEdit(btn_e);
};

function showEdit(btn_e) {
  let display = document.querySelector('.c-alter__display');
  let form = document.querySelector('.c-alter__form');
  let btn_c = document.querySelector('.c-btn__cancel');
  let btn_s = document.querySelector('.c-btn__save');

  display.style.display = 'none';
  form.style.display = 'block';
  btn_e.style.display = 'none';
  btn_c.style.display = 'block';
  btn_s.style.display = 'block';
}

function hideEdit() {
  let display = document.querySelector('.c-alter__display');
  let form = document.querySelector('.c-alter__form');
  let btn_c = document.querySelector('.c-btn__cancel');
  let btn_s = document.querySelector('.c-btn__save');
  let btn_e = document.querySelector('.c-btn__edit');

  display.style.display = 'block';
  form.style.display = 'none';
  btn_c.style.display = 'none';
  btn_s.style.display = 'none';
  btn_e.style.display = 'block';
}

// hide edit
const btn_c = document.querySelector('.c-btn__cancel');
btn_c.onclick = () => {
  hideEdit();
};

const btn_s = document.querySelector('.c-btn__save');
btn_s.onclick = () => {
  hideEdit();
};

// nav links
[].forEach.call(document.querySelectorAll('.c-link'), el => {
  el.onclick = () => {
    navigate(el);
  };
});

function navigate(el) {
  let tab = el.innerHTML;
  let active = document.querySelector('.active');
  let title = document.querySelector('.c-layout h3');
  let edit = document.querySelector('.c-btn__edit');
  let cancel = document.querySelector('.c-btn__cancel');
  let save = document.querySelector('.c-btn__save');
  let display = document.querySelector('.c-alter__display');
  let form = document.querySelector('.c-alter__form');

  if (tab === 'About') {
    if (window.innerWidth < 768) {
      edit.style.display = 'block';
    }
    console.log(window.innerWidth < 768);

    display.style.display = 'block';
  } else {
    edit.style.display = 'none';
    cancel.style.display = 'none';
    save.style.display = 'none';
    display.style.display = 'none';
    form.style.display = 'none';
  }

  title.innerHTML = tab;
  active.classList.remove('active');
  el.classList.add('active');
}


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vZGlzdC9zdHlsZS5jc3M/NWQ5NCIsIndlYnBhY2s6Ly8vLi9kaXN0L3N0eWxlLmNzcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9kaXN0L3J1bnRpbWUvYXBpLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9zdHlsZS1sb2FkZXIvZGlzdC9ydW50aW1lL2luamVjdFN0eWxlc0ludG9TdHlsZVRhZy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL2FkZC1jaXJjbGUuc3ZnIiwid2VicGFjazovLy8uL3NyYy9hc3NldHMvY2FsbC1vdXRsaW5lLnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL2dsb2JlLW91dGxpbmUuc3ZnIiwid2VicGFjazovLy8uL3NyYy9hc3NldHMvaG9tZS1vdXRsaW5lLnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL2xvY2F0aW9uLW91dGxpbmUuc3ZnIiwid2VicGFjazovLy8uL3NyYy9hc3NldHMvcGVuY2lsLW91dGxpbmUuc3ZnIiwid2VicGFjazovLy8uL3NyYy9hc3NldHMvc3Rhci1vdXRsaW5lLnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL3N0YXIuc3ZnIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL2hlYWRlci9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9oZWFkZXIvbWVudS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9oZWFkZXIvcHJvZmlsZV9pbWFnZS5qcGciLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvbWFpbi9mb3JtL2dyb3VwL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL21haW4vZm9ybS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9tYWluL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9pbmRleC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7QUNsRkEsVUFBVSxtQkFBTyxDQUFDLG1KQUF3RTtBQUMxRiwwQkFBMEIsbUJBQU8sQ0FBQyxrTUFBOEY7O0FBRWhJOztBQUVBO0FBQ0EsMEJBQTBCLFFBQVM7QUFDbkM7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7OztBQUlBLDBCOzs7Ozs7Ozs7OztBQ3BCQTtBQUNBLGtDQUFrQyxtQkFBTyxDQUFDLHFHQUFnRDtBQUMxRjtBQUNBO0FBQ0EsY0FBYyxRQUFTLGVBQWUsNkJBQTZCLHVCQUF1QixxQkFBcUIsdUJBQXVCLHVHQUF1RyxHQUFHLEtBQUsseUNBQXlDLGNBQWMsZUFBZSxHQUFHLFVBQVUsOEJBQThCLEdBQUcsZ0JBQWdCLG1CQUFtQixxQkFBcUIsR0FBRyxvQkFBb0IsbUJBQW1CLEdBQUcsNEJBQTRCLGtCQUFrQixHQUFHLDJCQUEyQixxQkFBcUIsR0FBRyxtQkFBbUIsbUJBQW1CLHFCQUFxQix5QkFBeUIseUJBQXlCLGtCQUFrQixHQUFHLHlDQUF5QyxxQkFBcUIsdUJBQXVCLEdBQUcsZ0ZBQWdGLDhCQUE4QiwyQkFBMkIsd0JBQXdCLEdBQUcsaUNBQWlDLHVCQUF1QixHQUFHLGFBQWEsbUJBQW1CLGtCQUFrQixHQUFHLCtCQUErQixnQkFBZ0IsbUJBQW1CLEtBQUssR0FBRyw2QkFBNkIsZ0JBQWdCLG1CQUFtQixLQUFLLEdBQUcsNkJBQTZCLGdCQUFnQixvQkFBb0IsS0FBSyxHQUFHLGFBQWEsMkJBQTJCLHNCQUFzQixHQUFHLGtDQUFrQyx3QkFBd0IsMkJBQTJCLEdBQUcseUJBQXlCLGdCQUFnQixpQkFBaUIsdUJBQXVCLHNCQUFzQixHQUFHLGdDQUFnQyxvQkFBb0IsR0FBRywrQkFBK0IsZ0JBQWdCLG1CQUFtQixLQUFLLDJCQUEyQixnQkFBZ0IsNkJBQTZCLHFCQUFxQix1QkFBdUIsZ0NBQWdDLEtBQUssdUNBQXVDLHVCQUF1QixxQkFBcUIsa0JBQWtCLEtBQUssd0NBQXdDLG9CQUFvQixLQUFLLHNDQUFzQyx3QkFBd0Isb0JBQW9CLGlDQUFpQyw2Q0FBNkMsd0JBQXdCLEtBQUssb0NBQW9DLDJCQUEyQixtQkFBbUIsS0FBSyxpREFBaUQsaUJBQWlCLHFCQUFxQix3QkFBd0IseUJBQXlCLEtBQUssd0NBQXdDLGtCQUFrQix5QkFBeUIsS0FBSyxvQ0FBb0MsMkJBQTJCLEtBQUssaURBQWlELHlCQUF5Qix3QkFBd0Isb0JBQW9CLGlDQUFpQyw2Q0FBNkMsa0JBQWtCLHdCQUF3QixLQUFLLDJEQUEyRCwyQkFBMkIsS0FBSyxxRkFBcUYsK0JBQStCLDRCQUE0Qiw4QkFBOEIsS0FBSywyREFBMkQsMkJBQTJCLEtBQUsseUVBQXlFLHlCQUF5QixLQUFLLG9GQUFvRix3QkFBd0IsS0FBSyxvREFBb0QseUJBQXlCLDJCQUEyQiwyQkFBMkIsb0JBQW9CLGdDQUFnQyw2QkFBNkIsMEJBQTBCLEtBQUssb0JBQW9CLG9CQUFvQixLQUFLLEdBQUcsNkJBQTZCLGdCQUFnQixtQkFBbUIsS0FBSyx1Q0FBdUMsdUJBQXVCLHFCQUFxQixrQkFBa0IsS0FBSyx3Q0FBd0MsdUJBQXVCLEtBQUssc0NBQXNDLHdCQUF3QixvQkFBb0IsaUNBQWlDLDZDQUE2Qyx3QkFBd0IsS0FBSyxvQ0FBb0MsMEJBQTBCLG1CQUFtQixLQUFLLGlEQUFpRCxpQkFBaUIscUJBQXFCLHdCQUF3Qix5QkFBeUIsS0FBSyx3Q0FBd0Msa0JBQWtCLHlCQUF5QixLQUFLLG9DQUFvQywwQkFBMEIsS0FBSyxpREFBaUQsd0JBQXdCLG9CQUFvQixpQ0FBaUMsNkNBQTZDLGtCQUFrQix3QkFBd0IsS0FBSywyREFBMkQsMEJBQTBCLEtBQUsscUZBQXFGLDZCQUE2QiwwQkFBMEIsNEJBQTRCLEtBQUssMkRBQTJELDBCQUEwQix3QkFBd0Isb0JBQW9CLGlDQUFpQyw2Q0FBNkMsS0FBSyx5RUFBeUUsMEJBQTBCLHlCQUF5QixLQUFLLG9GQUFvRix3QkFBd0IsS0FBSyxvREFBb0QseUJBQXlCLDJCQUEyQiwyQkFBMkIsb0JBQW9CLGdDQUFnQyw2QkFBNkIsMEJBQTBCLG1CQUFtQixrQkFBa0IseUJBQXlCLEtBQUssb0JBQW9CLHlCQUF5QixpQkFBaUIsb0JBQW9CLDZCQUE2QixxQkFBcUIsb0JBQW9CLEtBQUssR0FBRyxVQUFVLHlCQUF5Qix5QkFBeUIsa0JBQWtCLDhCQUE4QiwyQkFBMkIsd0JBQXdCLDhCQUE4QiwyQkFBMkIsbUNBQW1DLEdBQUcsZUFBZSx5QkFBeUIseUJBQXlCLGtCQUFrQiwwQkFBMEIsR0FBRyxlQUFlLGtCQUFrQixHQUFHLHFCQUFxQixvQkFBb0IsR0FBRywwQkFBMEIsa0JBQWtCLG1CQUFtQixtQkFBbUIsc0JBQXNCLG1DQUFtQyxHQUFHLCtCQUErQixZQUFZLGlDQUFpQyx1QkFBdUIsdUJBQXVCLEtBQUssaUJBQWlCLHdCQUF3QixLQUFLLEdBQUcsNkJBQTZCLFlBQVksZ0JBQWdCLHFCQUFxQixvQkFBb0IsS0FBSyxpQkFBaUIsd0JBQXdCLEtBQUssR0FBRyxXQUFXLDJCQUEyQixHQUFHLGlCQUFpQiwwQkFBMEIsdUJBQXVCLEdBQUcsK0JBQStCLGdCQUFnQixtQkFBbUIsS0FBSyxlQUFlLHdCQUF3QixvQkFBb0IsaUNBQWlDLDZDQUE2Qyx5QkFBeUIsS0FBSyxxQ0FBcUMsMkJBQTJCLDJCQUEyQixLQUFLLDRCQUE0Qiw0QkFBNEIsS0FBSyxnQ0FBZ0MsbUJBQW1CLHVCQUF1QixLQUFLLHdFQUF3RSxvQkFBb0IsZ0JBQWdCLDZCQUE2QixxQkFBcUIsdUJBQXVCLG9CQUFvQix5QkFBeUIsS0FBSyxzRkFBc0Ysc0JBQXNCLEtBQUssb0RBQW9ELDBCQUEwQixLQUFLLHVCQUF1QiwyQkFBMkIsMkJBQTJCLG9CQUFvQixnQ0FBZ0MsNkJBQTZCLDBCQUEwQixxQkFBcUIsdUJBQXVCLEtBQUssK0JBQStCLG9CQUFvQixLQUFLLG1DQUFtQyx3QkFBd0IsdUJBQXVCLEtBQUssc0JBQXNCLG9CQUFvQixLQUFLLEdBQUcsNkJBQTZCLGdCQUFnQixtQkFBbUIsS0FBSyxlQUFlLHdCQUF3QixvQkFBb0IsaUNBQWlDLDZDQUE2Qyx5QkFBeUIsS0FBSyxxQ0FBcUMsMkJBQTJCLDJCQUEyQixLQUFLLDRDQUE0QyxvQkFBb0IsS0FBSyxvREFBb0QsMEJBQTBCLEtBQUssdUJBQXVCLDJCQUEyQiwyQkFBMkIsb0JBQW9CLGdDQUFnQyw2QkFBNkIsMEJBQTBCLHFCQUFxQix1QkFBdUIsS0FBSywrQkFBK0IscUJBQXFCLEtBQUssbUNBQW1DLHdCQUF3Qix1QkFBdUIsS0FBSyxHQUFHLHVCQUF1QixxQ0FBcUMsaUNBQWlDLHlCQUF5QixHQUFHLHlDQUF5QyxtQkFBbUIsb0JBQW9CLHlDQUF5QyxpQ0FBaUMsR0FBRyx5Q0FBeUMsb0JBQW9CLHlDQUF5QyxpQ0FBaUMsR0FBRyxxQkFBcUIsbUJBQW1CLG9CQUFvQix3QkFBd0IsZ0JBQWdCLFlBQVksY0FBYyx5QkFBeUIsdUJBQXVCLFdBQVcsaUNBQWlDLHlCQUF5QixnQkFBZ0IsR0FBRyxxQkFBcUIsK0JBQStCLHFCQUFxQix1QkFBdUIsZ0JBQWdCLEdBQUcsNEJBQTRCLHFDQUFxQyxjQUFjLGtCQUFrQixtQkFBbUIsWUFBWSxtQkFBbUIsdUJBQXVCLGFBQWEsaUNBQWlDLHlCQUF5QixpQ0FBaUMseUJBQXlCLGNBQWMsR0FBRyxxQkFBcUIsNkJBQTZCLDBCQUEwQixxQkFBcUIsNEJBQTRCLGNBQWMsa0NBQWtDLGdCQUFnQixtQkFBbUIsb0JBQW9CLHFCQUFxQixlQUFlLGlDQUFpQyxnQkFBZ0IsR0FBRztBQUM1eFU7QUFDQTs7Ozs7Ozs7Ozs7OztBQ05hOztBQUViO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCOztBQUVoQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw0Q0FBNEMscUJBQXFCO0FBQ2pFOztBQUVBO0FBQ0EsS0FBSztBQUNMLElBQUk7QUFDSjs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLHFCQUFxQixpQkFBaUI7QUFDdEM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLG9CQUFvQixxQkFBcUI7QUFDekM7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLDhCQUE4Qjs7QUFFOUI7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQSxDQUFDOzs7QUFHRDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsY0FBYztBQUNuRTtBQUNBLEM7Ozs7Ozs7Ozs7OztBQzdGYTs7QUFFYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdURBQXVEOztBQUV2RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTs7QUFFQSxpQkFBaUIsd0JBQXdCO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsaUJBQWlCLGlCQUFpQjtBQUNsQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsZ0JBQWdCLEtBQXdDLEdBQUcsc0JBQWlCLEdBQUcsU0FBSTs7QUFFbkY7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0EscUVBQXFFLHFCQUFxQixhQUFhOztBQUV2Rzs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0EseURBQXlEO0FBQ3pELEdBQUc7O0FBRUg7OztBQUdBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSwwQkFBMEI7QUFDMUI7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxtQkFBbUIsNEJBQTRCO0FBQy9DO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLG9CQUFvQiw2QkFBNkI7QUFDakQ7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEU7Ozs7Ozs7Ozs7OztBQzVRQTtBQUFlLG9GQUF1Qix5Q0FBeUMsRTs7Ozs7Ozs7Ozs7O0FDQS9FO0FBQWUsb0ZBQXVCLHlDQUF5QyxFOzs7Ozs7Ozs7Ozs7QUNBL0U7QUFBZSxvRkFBdUIseUNBQXlDLEU7Ozs7Ozs7Ozs7OztBQ0EvRTtBQUFlLG9GQUF1Qix5Q0FBeUMsRTs7Ozs7Ozs7Ozs7O0FDQS9FO0FBQWUsb0ZBQXVCLHlDQUF5QyxFOzs7Ozs7Ozs7Ozs7QUNBL0U7QUFBZSxvRkFBdUIseUNBQXlDLEU7Ozs7Ozs7Ozs7OztBQ0EvRTtBQUFlLG9GQUF1Qix5Q0FBeUMsRTs7Ozs7Ozs7Ozs7O0FDQS9FO0FBQWUsb0ZBQXVCLHlDQUF5QyxFOzs7Ozs7Ozs7Ozs7QUNBL0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXlDO0FBQ1U7QUFDVjtBQUNLO0FBQ0c7QUFDRzs7QUFFYzs7QUFFbEU7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxjQUFjLDBEQUFNOztBQUVwQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQix3REFBSTtBQUNyQjtBQUNBLGlCQUFpQix3REFBSTtBQUNyQjtBQUNBLGlCQUFpQix3REFBSTtBQUNyQjtBQUNBLGlCQUFpQix3REFBSTtBQUNyQjtBQUNBO0FBQ0EsZ0JBQWdCLGdFQUFNO0FBQ3RCO0FBQ0E7QUFDQSxlQUFlLDhEQUFHO0FBQ2xCO0FBQ0E7QUFDQSxlQUFlLG9FQUFHO0FBQ2xCO0FBQ0E7QUFDQSxlQUFlLGdFQUFJOztBQUVuQjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxtQkFBbUIsbUVBQWU7O0FBRWxDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0E7O0FBRXdCOzs7Ozs7Ozs7Ozs7O0FDakt4QjtBQUFBO0FBQUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ3dCOzs7Ozs7Ozs7Ozs7O0FDbkV4QjtBQUFlLG9GQUF1Qix5Q0FBeUMsRTs7Ozs7Ozs7Ozs7O0FDQS9FO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFd0U7Ozs7Ozs7Ozs7Ozs7QUM3RHhFO0FBQUE7QUFBQTtBQU0wQjs7QUFFMUI7QUFDQTs7QUFFQSxtQkFBbUIsb0VBQVk7QUFDL0IsbUJBQW1CLG1FQUFXO0FBQzlCLG1CQUFtQixtRUFBVztBQUM5QixtQkFBbUIsZ0VBQVE7QUFDM0IsbUJBQW1CLGtFQUFVOztBQUU3QjtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTs7QUFFd0I7Ozs7Ozs7Ozs7Ozs7QUN0Q3hCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQW1EO0FBQ0Y7QUFDQTtBQUNJO0FBQ2M7O0FBRW5FO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxrQkFBa0IsaUVBQUs7O0FBRXZCO0FBQ0EsaUJBQWlCLGdFQUFJOztBQUVyQjtBQUNBLGlCQUFpQixnRUFBSTs7QUFFckI7QUFDQSxxQkFBcUIsa0VBQU07O0FBRTNCO0FBQ0EscUJBQXFCLGtFQUFNOztBQUUzQjtBQUNBLHFCQUFxQixrRUFBTTs7QUFFM0I7QUFDQSxxQkFBcUIsa0VBQU07O0FBRTNCO0FBQ0EscUJBQXFCLGtFQUFNOztBQUUzQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbUJBQW1CLG1FQUFnQjs7QUFFbkM7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTs7QUFFd0I7Ozs7Ozs7Ozs7Ozs7QUMxSXhCO0FBQUE7QUFBQTtBQUFrRjtBQUNKO0FBQzlFLG1CQUFPLENBQUMsMkNBQW1COztBQUUzQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLHNCQUFzQixnRkFBa0I7O0FBRXhDO0FBQ0Esc0JBQXNCLDhFQUFnQjs7QUFFdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiaW5kZXguYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvaW5kZXguanNcIik7XG4iLCJ2YXIgYXBpID0gcmVxdWlyZShcIiEuLi9ub2RlX21vZHVsZXMvc3R5bGUtbG9hZGVyL2Rpc3QvcnVudGltZS9pbmplY3RTdHlsZXNJbnRvU3R5bGVUYWcuanNcIik7XG4gICAgICAgICAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvY2pzLmpzIS4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9kaXN0L2Nqcy5qcyEuL3N0eWxlLmNzc1wiKTtcblxuICAgICAgICAgICAgY29udGVudCA9IGNvbnRlbnQuX19lc01vZHVsZSA/IGNvbnRlbnQuZGVmYXVsdCA6IGNvbnRlbnQ7XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICAgICAgfVxuXG52YXIgb3B0aW9ucyA9IHt9O1xuXG5vcHRpb25zLmluc2VydCA9IFwiaGVhZFwiO1xub3B0aW9ucy5zaW5nbGV0b24gPSBmYWxzZTtcblxudmFyIHVwZGF0ZSA9IGFwaShjb250ZW50LCBvcHRpb25zKTtcblxudmFyIGV4cG9ydGVkID0gY29udGVudC5sb2NhbHMgPyBjb250ZW50LmxvY2FscyA6IHt9O1xuXG5cblxubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRlZDsiLCIvLyBJbXBvcnRzXG52YXIgX19fQ1NTX0xPQURFUl9BUElfSU1QT1JUX19fID0gcmVxdWlyZShcIi4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvcnVudGltZS9hcGkuanNcIik7XG5leHBvcnRzID0gX19fQ1NTX0xPQURFUl9BUElfSU1QT1JUX19fKGZhbHNlKTtcbi8vIE1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiQGZvbnQtZmFjZSB7XFxuICBmb250LWZhbWlseTogXFxcIlJhbGV3YXlcXFwiO1xcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xcbiAgZm9udC13ZWlnaHQ6IDQwMDtcXG4gIGZvbnQtZGlzcGxheTogc3dhcDtcXG4gIHNyYzogdXJsKGh0dHBzOi8vZm9udHMuZ3N0YXRpYy5jb20vcy9yYWxld2F5L3YxNC8xUHR1Zzh6WVNfU0tnZ1BOeUNNSVQ1bHUud29mZjIpIGZvcm1hdChcXFwid29mZjJcXFwiKTtcXG59XFxuKiB7XFxuICBmb250LWZhbWlseTogXFxcIlJhbGV3YXlcXFwiLCBzYW5zLXNlcmlmO1xcbiAgbWFyZ2luOiAwO1xcbiAgcGFkZGluZzogMDtcXG59XFxuXFxuYm9keSB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjJmMmYyO1xcbn1cXG5cXG4uY29udGFpbmVyIHtcXG4gIG1hcmdpbjogMCBhdXRvO1xcbiAgcGFkZGluZzogMS4yNXJlbTtcXG59XFxuXFxuLmNvbnRhaW5lciA+ICoge1xcbiAgbWFyZ2luOiAycmVtIDA7XFxufVxcblxcbi5jb250YWluZXI6Zmlyc3QtY2hpbGQge1xcbiAgbWFyZ2luLXRvcDogMDtcXG59XFxuXFxuLmNvbnRhaW5lcjpsYXN0LWNoaWxkIHtcXG4gIG1hcmdpbi1ib3R0b206IDA7XFxufVxcblxcbi5jLWluZm9fX2xpc3Qge1xcbiAgcGFkZGluZzogNnB4IDA7XFxuICBmb250LXNpemU6IHNtYWxsO1xcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxufVxcblxcbi5jLWNvbF9fNyAuYy1pbmZvX19saXN0OmZpcnN0LWNoaWxkIHtcXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XFxuICBmb250LXNpemU6IHgtbGFyZ2U7XFxufVxcblxcbi5jLWNvbF9fNyAuYy1pbmZvX19saXN0Om50aC1jaGlsZCgyKSwgLmMtY29sX183IC5jLWluZm9fX2xpc3Q6bnRoLWNoaWxkKDMpIHtcXG4gIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XFxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG59XFxuXFxuLmMtY29sX183IC5jLWluZm9fX2xpc3QgPiAqIHtcXG4gIHBhZGRpbmctcmlnaHQ6IDhweDtcXG59XFxuXFxuLmMtaWNvbiB7XFxuICBoZWlnaHQ6IDEuM3JlbTtcXG4gIHdpZHRoOiAxLjNyZW07XFxufVxcblxcbkBtZWRpYSAobWF4LXdpZHRoOiA3NjhweCkge1xcbiAgLmNvbnRhaW5lciB7XFxuICAgIHdpZHRoOiA3NTBweDtcXG4gIH1cXG59XFxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XFxuICAuY29udGFpbmVyIHtcXG4gICAgd2lkdGg6IDk3MHB4O1xcbiAgfVxcbn1cXG5AbWVkaWEgKG1pbi13aWR0aDogOTkycHgpIHtcXG4gIC5jb250YWluZXIge1xcbiAgICB3aWR0aDogMTE3MHB4O1xcbiAgfVxcbn1cXG4uYy1oZWFkZXIge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIG1pbi1oZWlnaHQ6IDI0MHB4O1xcbn1cXG5cXG4uYy1oZWFkZXIgLmMtYnV0dG9uX193cmFwcGVyIHtcXG4gIGRpc3BsYXk6IHRhYmxlLWNlbGw7XFxuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xcbn1cXG5cXG4uYy1oZWFkZXIgLmMtYnV0dG9uIHtcXG4gIHJpZ2h0OiAxNXB4O1xcbiAgZmxvYXQ6IHJpZ2h0O1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgcGFkZGluZzogNXB4IDEycHg7XFxufVxcblxcbi5jLWhlYWRlciAuYy1idXR0b24gOmhvdmVyIHtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG59XFxuXFxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XFxuICAuY29udGFpbmVyIHtcXG4gICAgd2lkdGg6IDU0MHB4O1xcbiAgfVxcblxcbiAgLmMtaGVhZGVyIC5jLWJ1dHRvbiB7XFxuICAgIGJvcmRlcjogMDtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gICAgY29sb3I6ICMxZTg4ZTU7XFxuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XFxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XFxuICB9XFxuXFxuICAuYy1oZWFkZXIgLmMtbGF5b3V0OmZpcnN0LWNoaWxkIHtcXG4gICAgbWluLWhlaWdodDogNjVweDtcXG4gICAgZGlzcGxheTogdGFibGU7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcblxcbiAgLmMtaGVhZGVyIC5jLWxheW91dDpudGgtY2hpbGQoMikge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLmMtaGVhZGVyIC5jLWxheW91dDpsYXN0LWNoaWxkIHtcXG4gICAgZGlzcGxheTogLW1zLWdyaWQ7XFxuICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgIC1tcy1ncmlkLWNvbHVtbnM6IDFmciBbMTJdO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgxMiwgMWZyKTtcXG4gICAgbWluLWhlaWdodDogMjAwcHg7XFxuICB9XFxuXFxuICAuYy1oZWFkZXIgLmMtY29udGFpbmVyX18yNS1kIHtcXG4gICAgZ3JpZC1jb2x1bW46IHNwYW4gMTI7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC5jLWhlYWRlciAuYy1jb250YWluZXJfXzI1LWQgLmMtY29udGFpbmVyIHtcXG4gICAgd2lkdGg6IDgwJTtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XFxuICB9XFxuXFxuICAuYy1oZWFkZXIgLmMtY29udGFpbmVyX18yNS1kIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICB9XFxuXFxuICAuYy1oZWFkZXIgLmMtY29udGFpbmVyX183NS1kIHtcXG4gICAgZ3JpZC1jb2x1bW46IHNwYW4gMTI7XFxuICB9XFxuXFxuICAuYy1oZWFkZXIgLmMtY29udGFpbmVyX183NS1kIC5jLWNvbnRhaW5lciB7XFxuICAgIHBhZGRpbmctbGVmdDogMTVweDtcXG4gICAgZGlzcGxheTogLW1zLWdyaWQ7XFxuICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgIC1tcy1ncmlkLWNvbHVtbnM6IDFmciBbMTJdO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgxMiwgMWZyKTtcXG4gICAgaGVpZ2h0OiA2NSU7XFxuICAgIHBhZGRpbmctdG9wOiAyNXB4O1xcbiAgfVxcblxcbiAgLmMtaGVhZGVyIC5jLWNvbnRhaW5lcl9fNzUtZCAuYy1jb250YWluZXIgLmMtY29sX183IHtcXG4gICAgZ3JpZC1jb2x1bW46IHNwYW4gMTI7XFxuICB9XFxuXFxuICAuYy1oZWFkZXIgLmMtY29udGFpbmVyX183NS1kIC5jLWNvbnRhaW5lciAuYy1jb2xfXzcgLmMtaW5mb19fbGlzdDpmaXJzdC1jaGlsZCB7XFxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcXG4gICAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIH1cXG5cXG4gIC5jLWhlYWRlciAuYy1jb250YWluZXJfXzc1LWQgLmMtY29udGFpbmVyIC5jLWNvbF9fNSB7XFxuICAgIGdyaWQtY29sdW1uOiBzcGFuIDEyO1xcbiAgfVxcblxcbiAgLmMtaGVhZGVyIC5jLWNvbnRhaW5lcl9fNzUtZCAuYy1jb250YWluZXIgLmMtY29sX181IC5jLWluZm9fX2xpc3Qge1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICB9XFxuXFxuICAuYy1oZWFkZXIgLmMtY29udGFpbmVyX183NS1kIC5jLWNvbnRhaW5lciAuYy1jb2xfXzUgLmMtaW5mb19fbGlzdDpsYXN0LWNoaWxkIHtcXG4gICAgcGFkZGluZy10b3A6IDEycHg7XFxuICB9XFxuXFxuICAuYy1oZWFkZXIgLmMtY29udGFpbmVyX183NS1kID4gLmMtaW5mb19fbGlzdCB7XFxuICAgIHBhZGRpbmctbGVmdDogMTVweDtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgfVxcblxcbiAgLmMtaGVhZGVyIGhyIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG59XFxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XFxuICAuY29udGFpbmVyIHtcXG4gICAgd2lkdGg6IDcyMHB4O1xcbiAgfVxcblxcbiAgLmMtaGVhZGVyIC5jLWxheW91dDpmaXJzdC1jaGlsZCB7XFxuICAgIG1pbi1oZWlnaHQ6IDY1cHg7XFxuICAgIGRpc3BsYXk6IHRhYmxlO1xcbiAgICB3aWR0aDogMTAwJTtcXG4gIH1cXG5cXG4gIC5jLWhlYWRlciAuYy1sYXlvdXQ6bnRoLWNoaWxkKDIpIHtcXG4gICAgbWluLWhlaWdodDogNTBweDtcXG4gIH1cXG5cXG4gIC5jLWhlYWRlciAuYy1sYXlvdXQ6bGFzdC1jaGlsZCB7XFxuICAgIGRpc3BsYXk6IC1tcy1ncmlkO1xcbiAgICBkaXNwbGF5OiBncmlkO1xcbiAgICAtbXMtZ3JpZC1jb2x1bW5zOiAxZnIgWzEyXTtcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMTIsIDFmcik7XFxuICAgIG1pbi1oZWlnaHQ6IDIwMHB4O1xcbiAgfVxcblxcbiAgLmMtaGVhZGVyIC5jLWNvbnRhaW5lcl9fMjUtZCB7XFxuICAgIGdyaWQtY29sdW1uOiBzcGFuIDM7XFxuICAgIG1hcmdpbjogYXV0bztcXG4gIH1cXG5cXG4gIC5jLWhlYWRlciAuYy1jb250YWluZXJfXzI1LWQgLmMtY29udGFpbmVyIHtcXG4gICAgd2lkdGg6IDgwJTtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XFxuICB9XFxuXFxuICAuYy1oZWFkZXIgLmMtY29udGFpbmVyX18yNS1kIGltZyB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICB9XFxuXFxuICAuYy1oZWFkZXIgLmMtY29udGFpbmVyX183NS1kIHtcXG4gICAgZ3JpZC1jb2x1bW46IHNwYW4gOTtcXG4gIH1cXG5cXG4gIC5jLWhlYWRlciAuYy1jb250YWluZXJfXzc1LWQgLmMtY29udGFpbmVyIHtcXG4gICAgZGlzcGxheTogLW1zLWdyaWQ7XFxuICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgIC1tcy1ncmlkLWNvbHVtbnM6IDFmciBbMTJdO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgxMiwgMWZyKTtcXG4gICAgaGVpZ2h0OiA2NSU7XFxuICAgIHBhZGRpbmctdG9wOiAyNXB4O1xcbiAgfVxcblxcbiAgLmMtaGVhZGVyIC5jLWNvbnRhaW5lcl9fNzUtZCAuYy1jb250YWluZXIgLmMtY29sX183IHtcXG4gICAgZ3JpZC1jb2x1bW46IHNwYW4gNztcXG4gIH1cXG5cXG4gIC5jLWhlYWRlciAuYy1jb250YWluZXJfXzc1LWQgLmMtY29udGFpbmVyIC5jLWNvbF9fNyAuYy1pbmZvX19saXN0OmZpcnN0LWNoaWxkIHtcXG4gICAgLXdlYmtpdC1ib3gtcGFjazogbGVmdDtcXG4gICAgLW1zLWZsZXgtcGFjazogbGVmdDtcXG4gICAganVzdGlmeS1jb250ZW50OiBsZWZ0O1xcbiAgfVxcblxcbiAgLmMtaGVhZGVyIC5jLWNvbnRhaW5lcl9fNzUtZCAuYy1jb250YWluZXIgLmMtY29sX181IHtcXG4gICAgZ3JpZC1jb2x1bW46IHNwYW4gNTtcXG4gICAgZGlzcGxheTogLW1zLWdyaWQ7XFxuICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgIC1tcy1ncmlkLWNvbHVtbnM6IDFmciBbMTJdO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgxMiwgMWZyKTtcXG4gIH1cXG5cXG4gIC5jLWhlYWRlciAuYy1jb250YWluZXJfXzc1LWQgLmMtY29udGFpbmVyIC5jLWNvbF9fNSAuYy1pbmZvX19saXN0IHtcXG4gICAgZ3JpZC1jb2x1bW46IHNwYW4gNjtcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgfVxcblxcbiAgLmMtaGVhZGVyIC5jLWNvbnRhaW5lcl9fNzUtZCAuYy1jb250YWluZXIgLmMtY29sX181IC5jLWluZm9fX2xpc3Q6bGFzdC1jaGlsZCB7XFxuICAgIHBhZGRpbmctdG9wOiAxMnB4O1xcbiAgfVxcblxcbiAgLmMtaGVhZGVyIC5jLWNvbnRhaW5lcl9fNzUtZCA+IC5jLWluZm9fX2xpc3Qge1xcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XFxuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcXG4gICAgZGlzcGxheTogZmxleDtcXG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gICAgZmxvYXQ6IHJpZ2h0O1xcbiAgICByaWdodDogMjBweDtcXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgfVxcblxcbiAgLmMtaGVhZGVyIGhyIHtcXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgICB0b3A6IDE1MHB4O1xcbiAgICB6LWluZGV4OiBhdXRvO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwO1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgaGVpZ2h0OiAxLjVweDtcXG4gIH1cXG59XFxuLmMtbmF2IHtcXG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XFxuICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgLXdlYmtpdC1ib3gtcGFjazoganVzdGlmeTtcXG4gIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxufVxcblxcbi5jLW5hdiB1bCB7XFxuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcXG59XFxuXFxuLmMtbmF2IGxpIHtcXG4gIHBhZGRpbmc6IDEwcHg7XFxufVxcblxcbi5jLW5hdiBsaTpob3ZlciB7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxufVxcblxcbi5jLW5hdiAuYWN0aXZlOmFmdGVyIHtcXG4gIGNvbnRlbnQ6IFxcXCJcXFwiO1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBtYXJnaW46IDAgYXV0bztcXG4gIHBhZGRpbmctdG9wOiAxNXB4O1xcbiAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkIGJsYWNrO1xcbn1cXG5cXG5AbWVkaWEgKG1heC13aWR0aDogNzY4cHgpIHtcXG4gIC5jLW5hdiB7XFxuICAgIGJvcmRlci10b3A6IDJweCBzb2xpZCAjMDAwO1xcbiAgICBwYWRkaW5nLXRvcDogNXB4O1xcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xcbiAgfVxcblxcbiAgLmMtbmF2IGxpIHtcXG4gICAgcGFkZGluZy1ib3R0b206IDA7XFxuICB9XFxufVxcbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xcbiAgLmMtbmF2IHtcXG4gICAgYm9yZGVyOiAwO1xcbiAgICBwYWRkaW5nLXRvcDogMDtcXG4gICAgbWFyZ2luLXRvcDogMDtcXG4gIH1cXG5cXG4gIC5jLW5hdiBsaSB7XFxuICAgIHBhZGRpbmctYm90dG9tOiAwO1xcbiAgfVxcbn1cXG4uYy1tYWluIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XFxufVxcblxcbi5jLWl0ZW0gPiAqIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIHBhZGRpbmctcmlnaHQ6IDhweDtcXG59XFxuXFxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XFxuICAuY29udGFpbmVyIHtcXG4gICAgd2lkdGg6IDU0MHB4O1xcbiAgfVxcblxcbiAgLmMtbWFpbiB7XFxuICAgIGRpc3BsYXk6IC1tcy1ncmlkO1xcbiAgICBkaXNwbGF5OiBncmlkO1xcbiAgICAtbXMtZ3JpZC1jb2x1bW5zOiAxZnIgWzEyXTtcXG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiByZXBlYXQoMTIsIDFmcik7XFxuICAgIHBhZGRpbmc6IDI1cHggMjBweDtcXG4gIH1cXG5cXG4gIC5jLW1haW4gLmMtbGF5b3V0OmZpcnN0LWNoaWxkIHtcXG4gICAgZ3JpZC1jb2x1bW46IHNwYW4gMTI7XFxuICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xcbiAgfVxcblxcbiAgLmMtbWFpbiAuYy1sYXlvdXQgaDMge1xcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICB9XFxuXFxuICAuYy1tYWluIC5jLWxheW91dCBidXR0b24ge1xcbiAgICBmbG9hdDogcmlnaHQ7XFxuICAgIHBhZGRpbmc6IDVweCA4cHg7XFxuICB9XFxuXFxuICAuYy1tYWluIC5jLWxheW91dCAuYy1idG5fX2NhbmNlbCwgLmMtbWFpbiAuYy1sYXlvdXQgLmMtYnRuX19zYXZlIHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gICAgYm9yZGVyOiAwO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xcbiAgICBjb2xvcjogIzFlODhlNTtcXG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcXG4gICAgcGFkZGluZzogMTBweDtcXG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xcbiAgfVxcblxcbiAgLmMtbWFpbiAuYy1sYXlvdXQgLmMtYnRuX19jYW5jZWwgOmhvdmVyLCAuYy1tYWluIC5jLWxheW91dCAuYy1idG5fX3NhdmUgOmhvdmVyIHtcXG4gICAgY3Vyc29yOiBwb2ludGVyO1xcbiAgfVxcblxcbiAgLmMtbWFpbiAuYy1sYXlvdXQ6bnRoLWNoaWxkKDIpLCAuYy1tYWluIGZvcm0ge1xcbiAgICBncmlkLWNvbHVtbjogc3BhbiA2O1xcbiAgfVxcblxcbiAgLmMtbWFpbiAuYy1pdGVtIHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICBwYWRkaW5nOiA1cHggMDtcXG4gICAgZm9udC1zaXplOiBzbWFsbDtcXG4gIH1cXG5cXG4gIC5jLW1haW4gLmMtaXRlbSAuYy1lZGl0IHtcXG4gICAgZGlzcGxheTogbm9uZTtcXG4gIH1cXG5cXG4gIC5jLW1haW4gLmMtaXRlbTpmaXJzdC1jaGlsZCB7XFxuICAgIGZvbnQtc2l6ZTogbWVkaXVtO1xcbiAgICBmb250LXdlaWdodDogNzAwO1xcbiAgfVxcblxcbiAgLmMtYWx0ZXJfX2Zvcm0ge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcbn1cXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcXG4gIC5jb250YWluZXIge1xcbiAgICB3aWR0aDogNzIwcHg7XFxuICB9XFxuXFxuICAuYy1tYWluIHtcXG4gICAgZGlzcGxheTogLW1zLWdyaWQ7XFxuICAgIGRpc3BsYXk6IGdyaWQ7XFxuICAgIC1tcy1ncmlkLWNvbHVtbnM6IDFmciBbMTJdO1xcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdCgxMiwgMWZyKTtcXG4gICAgcGFkZGluZzogMjVweCAyMHB4O1xcbiAgfVxcblxcbiAgLmMtbWFpbiAuYy1sYXlvdXQ6Zmlyc3QtY2hpbGQge1xcbiAgICBncmlkLWNvbHVtbjogc3BhbiAxMjtcXG4gICAgcGFkZGluZy1ib3R0b206IDIwcHg7XFxuICB9XFxuXFxuICAuYy1tYWluIC5jLWxheW91dDpmaXJzdC1jaGlsZCBidXR0b24ge1xcbiAgICBkaXNwbGF5OiBub25lO1xcbiAgfVxcblxcbiAgLmMtbWFpbiAuYy1sYXlvdXQ6bnRoLWNoaWxkKDIpLCAuYy1tYWluIGZvcm0ge1xcbiAgICBncmlkLWNvbHVtbjogc3BhbiA2O1xcbiAgfVxcblxcbiAgLmMtbWFpbiAuYy1pdGVtIHtcXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XFxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xcbiAgICBkaXNwbGF5OiBmbGV4O1xcbiAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgICBwYWRkaW5nOiA1cHggMDtcXG4gICAgZm9udC1zaXplOiBzbWFsbDtcXG4gIH1cXG5cXG4gIC5jLW1haW4gLmMtaXRlbSAuYy1lZGl0IHtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICB9XFxuXFxuICAuYy1tYWluIC5jLWl0ZW06Zmlyc3QtY2hpbGQge1xcbiAgICBmb250LXNpemU6IG1lZGl1bTtcXG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcXG4gIH1cXG59XFxuLmMtaW5wdXQtLWlzLWFjdGl2ZSB7XFxuICBib3JkZXItYm90dG9tOiAycHggc29saWQgIzFlODhlNTtcXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuNHM7XFxuICB0cmFuc2l0aW9uOiBhbGwgMC40cztcXG59XFxuXFxuLmMtaW5wdXQtLWlzLWFjdGl2ZSAuYy1pbnB1dF9fbGFiZWwge1xcbiAgY29sb3I6ICMxZTg4ZTU7XFxuICBmb250LXNpemU6IDEycHg7XFxuICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWSgtMTRweCk7XFxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTE0cHgpO1xcbn1cXG5cXG4uYy1pbnB1dC0taXMtZmlsbGVkIC5jLWlucHV0X19sYWJlbCB7XFxuICBmb250LXNpemU6IDEycHg7XFxuICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWSgtMTRweCk7XFxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTE0cHgpO1xcbn1cXG5cXG4uYy1pbnB1dF9fbGFiZWwge1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBmb250LXNpemU6IDE2cHg7XFxuICBmb250LXdlaWdodDogbm9ybWFsO1xcbiAgY29sb3I6ICNjY2M7XFxuICBsZWZ0OiAwO1xcbiAgbWFyZ2luOiAwO1xcbiAgcGFkZGluZzogMThweCAxMnB4IDA7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDA7XFxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjRzO1xcbiAgdHJhbnNpdGlvbjogYWxsIDAuNHM7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuXFxuLmMtaW5wdXRfX2dyb3VwIHtcXG4gIGJvcmRlci1yYWRpdXM6IDhweCA4cHggMCAwO1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHdpZHRoOiAxMDAlO1xcbn1cXG5cXG4uYy1pbnB1dF9fZ3JvdXA6OmFmdGVyIHtcXG4gIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjMWU4OGU1O1xcbiAgYm90dG9tOiAwO1xcbiAgY29udGVudDogXFxcIlxcXCI7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIGxlZnQ6IDA7XFxuICBtYXJnaW46IDAgYXV0bztcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHJpZ2h0OiAwO1xcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlWCgwKTtcXG4gIHRyYW5zZm9ybTogc2NhbGVYKDApO1xcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC40cztcXG4gIHRyYW5zaXRpb246IGFsbCAwLjRzO1xcbiAgd2lkdGg6IDElO1xcbn1cXG5cXG4uYy1pbnB1dF9fZmllbGQge1xcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xcbiAgLW1vei1hcHBlYXJhbmNlOiBub25lO1xcbiAgYXBwZWFyYW5jZTogbm9uZTtcXG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xcbiAgYm9yZGVyOiAwO1xcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICM5OTk7XFxuICBjb2xvcjogIzMzMztcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgZm9udC1zaXplOiAxNnB4O1xcbiAgbWFyZ2luLXRvcDogMTBweDtcXG4gIG91dGxpbmU6IDA7XFxuICBwYWRkaW5nOiAxNHB4IDEycHggMTBweCAxMnB4O1xcbiAgd2lkdGg6IDEwMCU7XFxufVwiLCBcIlwiXSk7XG4vLyBFeHBvcnRzXG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHM7XG4iLCJcInVzZSBzdHJpY3RcIjtcblxuLypcbiAgTUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcbiAgQXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxuKi9cbi8vIGNzcyBiYXNlIGNvZGUsIGluamVjdGVkIGJ5IHRoZSBjc3MtbG9hZGVyXG4vLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgZnVuYy1uYW1lc1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAodXNlU291cmNlTWFwKSB7XG4gIHZhciBsaXN0ID0gW107IC8vIHJldHVybiB0aGUgbGlzdCBvZiBtb2R1bGVzIGFzIGNzcyBzdHJpbmdcblxuICBsaXN0LnRvU3RyaW5nID0gZnVuY3Rpb24gdG9TdHJpbmcoKSB7XG4gICAgcmV0dXJuIHRoaXMubWFwKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICB2YXIgY29udGVudCA9IGNzc1dpdGhNYXBwaW5nVG9TdHJpbmcoaXRlbSwgdXNlU291cmNlTWFwKTtcblxuICAgICAgaWYgKGl0ZW1bMl0pIHtcbiAgICAgICAgcmV0dXJuIFwiQG1lZGlhIFwiLmNvbmNhdChpdGVtWzJdLCBcIiB7XCIpLmNvbmNhdChjb250ZW50LCBcIn1cIik7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBjb250ZW50O1xuICAgIH0pLmpvaW4oJycpO1xuICB9OyAvLyBpbXBvcnQgYSBsaXN0IG9mIG1vZHVsZXMgaW50byB0aGUgbGlzdFxuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgZnVuYy1uYW1lc1xuXG5cbiAgbGlzdC5pID0gZnVuY3Rpb24gKG1vZHVsZXMsIG1lZGlhUXVlcnksIGRlZHVwZSkge1xuICAgIGlmICh0eXBlb2YgbW9kdWxlcyA9PT0gJ3N0cmluZycpIHtcbiAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1wYXJhbS1yZWFzc2lnblxuICAgICAgbW9kdWxlcyA9IFtbbnVsbCwgbW9kdWxlcywgJyddXTtcbiAgICB9XG5cbiAgICB2YXIgYWxyZWFkeUltcG9ydGVkTW9kdWxlcyA9IHt9O1xuXG4gICAgaWYgKGRlZHVwZSkge1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBwcmVmZXItZGVzdHJ1Y3R1cmluZ1xuICAgICAgICB2YXIgaWQgPSB0aGlzW2ldWzBdO1xuXG4gICAgICAgIGlmIChpZCAhPSBudWxsKSB7XG4gICAgICAgICAgYWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpZF0gPSB0cnVlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgZm9yICh2YXIgX2kgPSAwOyBfaSA8IG1vZHVsZXMubGVuZ3RoOyBfaSsrKSB7XG4gICAgICB2YXIgaXRlbSA9IFtdLmNvbmNhdChtb2R1bGVzW19pXSk7XG5cbiAgICAgIGlmIChkZWR1cGUgJiYgYWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpdGVtWzBdXSkge1xuICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tY29udGludWVcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG5cbiAgICAgIGlmIChtZWRpYVF1ZXJ5KSB7XG4gICAgICAgIGlmICghaXRlbVsyXSkge1xuICAgICAgICAgIGl0ZW1bMl0gPSBtZWRpYVF1ZXJ5O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGl0ZW1bMl0gPSBcIlwiLmNvbmNhdChtZWRpYVF1ZXJ5LCBcIiBhbmQgXCIpLmNvbmNhdChpdGVtWzJdKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBsaXN0LnB1c2goaXRlbSk7XG4gICAgfVxuICB9O1xuXG4gIHJldHVybiBsaXN0O1xufTtcblxuZnVuY3Rpb24gY3NzV2l0aE1hcHBpbmdUb1N0cmluZyhpdGVtLCB1c2VTb3VyY2VNYXApIHtcbiAgdmFyIGNvbnRlbnQgPSBpdGVtWzFdIHx8ICcnOyAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgcHJlZmVyLWRlc3RydWN0dXJpbmdcblxuICB2YXIgY3NzTWFwcGluZyA9IGl0ZW1bM107XG5cbiAgaWYgKCFjc3NNYXBwaW5nKSB7XG4gICAgcmV0dXJuIGNvbnRlbnQ7XG4gIH1cblxuICBpZiAodXNlU291cmNlTWFwICYmIHR5cGVvZiBidG9hID09PSAnZnVuY3Rpb24nKSB7XG4gICAgdmFyIHNvdXJjZU1hcHBpbmcgPSB0b0NvbW1lbnQoY3NzTWFwcGluZyk7XG4gICAgdmFyIHNvdXJjZVVSTHMgPSBjc3NNYXBwaW5nLnNvdXJjZXMubWFwKGZ1bmN0aW9uIChzb3VyY2UpIHtcbiAgICAgIHJldHVybiBcIi8qIyBzb3VyY2VVUkw9XCIuY29uY2F0KGNzc01hcHBpbmcuc291cmNlUm9vdCB8fCAnJykuY29uY2F0KHNvdXJjZSwgXCIgKi9cIik7XG4gICAgfSk7XG4gICAgcmV0dXJuIFtjb250ZW50XS5jb25jYXQoc291cmNlVVJMcykuY29uY2F0KFtzb3VyY2VNYXBwaW5nXSkuam9pbignXFxuJyk7XG4gIH1cblxuICByZXR1cm4gW2NvbnRlbnRdLmpvaW4oJ1xcbicpO1xufSAvLyBBZGFwdGVkIGZyb20gY29udmVydC1zb3VyY2UtbWFwIChNSVQpXG5cblxuZnVuY3Rpb24gdG9Db21tZW50KHNvdXJjZU1hcCkge1xuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tdW5kZWZcbiAgdmFyIGJhc2U2NCA9IGJ0b2EodW5lc2NhcGUoZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KHNvdXJjZU1hcCkpKSk7XG4gIHZhciBkYXRhID0gXCJzb3VyY2VNYXBwaW5nVVJMPWRhdGE6YXBwbGljYXRpb24vanNvbjtjaGFyc2V0PXV0Zi04O2Jhc2U2NCxcIi5jb25jYXQoYmFzZTY0KTtcbiAgcmV0dXJuIFwiLyojIFwiLmNvbmNhdChkYXRhLCBcIiAqL1wiKTtcbn0iLCJcInVzZSBzdHJpY3RcIjtcblxudmFyIGlzT2xkSUUgPSBmdW5jdGlvbiBpc09sZElFKCkge1xuICB2YXIgbWVtbztcbiAgcmV0dXJuIGZ1bmN0aW9uIG1lbW9yaXplKCkge1xuICAgIGlmICh0eXBlb2YgbWVtbyA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIC8vIFRlc3QgZm9yIElFIDw9IDkgYXMgcHJvcG9zZWQgYnkgQnJvd3NlcmhhY2tzXG4gICAgICAvLyBAc2VlIGh0dHA6Ly9icm93c2VyaGFja3MuY29tLyNoYWNrLWU3MWQ4NjkyZjY1MzM0MTczZmVlNzE1YzIyMmNiODA1XG4gICAgICAvLyBUZXN0cyBmb3IgZXhpc3RlbmNlIG9mIHN0YW5kYXJkIGdsb2JhbHMgaXMgdG8gYWxsb3cgc3R5bGUtbG9hZGVyXG4gICAgICAvLyB0byBvcGVyYXRlIGNvcnJlY3RseSBpbnRvIG5vbi1zdGFuZGFyZCBlbnZpcm9ubWVudHNcbiAgICAgIC8vIEBzZWUgaHR0cHM6Ly9naXRodWIuY29tL3dlYnBhY2stY29udHJpYi9zdHlsZS1sb2FkZXIvaXNzdWVzLzE3N1xuICAgICAgbWVtbyA9IEJvb2xlYW4od2luZG93ICYmIGRvY3VtZW50ICYmIGRvY3VtZW50LmFsbCAmJiAhd2luZG93LmF0b2IpO1xuICAgIH1cblxuICAgIHJldHVybiBtZW1vO1xuICB9O1xufSgpO1xuXG52YXIgZ2V0VGFyZ2V0ID0gZnVuY3Rpb24gZ2V0VGFyZ2V0KCkge1xuICB2YXIgbWVtbyA9IHt9O1xuICByZXR1cm4gZnVuY3Rpb24gbWVtb3JpemUodGFyZ2V0KSB7XG4gICAgaWYgKHR5cGVvZiBtZW1vW3RhcmdldF0gPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICB2YXIgc3R5bGVUYXJnZXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHRhcmdldCk7IC8vIFNwZWNpYWwgY2FzZSB0byByZXR1cm4gaGVhZCBvZiBpZnJhbWUgaW5zdGVhZCBvZiBpZnJhbWUgaXRzZWxmXG5cbiAgICAgIGlmICh3aW5kb3cuSFRNTElGcmFtZUVsZW1lbnQgJiYgc3R5bGVUYXJnZXQgaW5zdGFuY2VvZiB3aW5kb3cuSFRNTElGcmFtZUVsZW1lbnQpIHtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAvLyBUaGlzIHdpbGwgdGhyb3cgYW4gZXhjZXB0aW9uIGlmIGFjY2VzcyB0byBpZnJhbWUgaXMgYmxvY2tlZFxuICAgICAgICAgIC8vIGR1ZSB0byBjcm9zcy1vcmlnaW4gcmVzdHJpY3Rpb25zXG4gICAgICAgICAgc3R5bGVUYXJnZXQgPSBzdHlsZVRhcmdldC5jb250ZW50RG9jdW1lbnQuaGVhZDtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgIC8vIGlzdGFuYnVsIGlnbm9yZSBuZXh0XG4gICAgICAgICAgc3R5bGVUYXJnZXQgPSBudWxsO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIG1lbW9bdGFyZ2V0XSA9IHN0eWxlVGFyZ2V0O1xuICAgIH1cblxuICAgIHJldHVybiBtZW1vW3RhcmdldF07XG4gIH07XG59KCk7XG5cbnZhciBzdHlsZXNJbkRvbSA9IFtdO1xuXG5mdW5jdGlvbiBnZXRJbmRleEJ5SWRlbnRpZmllcihpZGVudGlmaWVyKSB7XG4gIHZhciByZXN1bHQgPSAtMTtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IHN0eWxlc0luRG9tLmxlbmd0aDsgaSsrKSB7XG4gICAgaWYgKHN0eWxlc0luRG9tW2ldLmlkZW50aWZpZXIgPT09IGlkZW50aWZpZXIpIHtcbiAgICAgIHJlc3VsdCA9IGk7XG4gICAgICBicmVhaztcbiAgICB9XG4gIH1cblxuICByZXR1cm4gcmVzdWx0O1xufVxuXG5mdW5jdGlvbiBtb2R1bGVzVG9Eb20obGlzdCwgb3B0aW9ucykge1xuICB2YXIgaWRDb3VudE1hcCA9IHt9O1xuICB2YXIgaWRlbnRpZmllcnMgPSBbXTtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IGxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgaXRlbSA9IGxpc3RbaV07XG4gICAgdmFyIGlkID0gb3B0aW9ucy5iYXNlID8gaXRlbVswXSArIG9wdGlvbnMuYmFzZSA6IGl0ZW1bMF07XG4gICAgdmFyIGNvdW50ID0gaWRDb3VudE1hcFtpZF0gfHwgMDtcbiAgICB2YXIgaWRlbnRpZmllciA9IFwiXCIuY29uY2F0KGlkLCBcIiBcIikuY29uY2F0KGNvdW50KTtcbiAgICBpZENvdW50TWFwW2lkXSA9IGNvdW50ICsgMTtcbiAgICB2YXIgaW5kZXggPSBnZXRJbmRleEJ5SWRlbnRpZmllcihpZGVudGlmaWVyKTtcbiAgICB2YXIgb2JqID0ge1xuICAgICAgY3NzOiBpdGVtWzFdLFxuICAgICAgbWVkaWE6IGl0ZW1bMl0sXG4gICAgICBzb3VyY2VNYXA6IGl0ZW1bM11cbiAgICB9O1xuXG4gICAgaWYgKGluZGV4ICE9PSAtMSkge1xuICAgICAgc3R5bGVzSW5Eb21baW5kZXhdLnJlZmVyZW5jZXMrKztcbiAgICAgIHN0eWxlc0luRG9tW2luZGV4XS51cGRhdGVyKG9iaik7XG4gICAgfSBlbHNlIHtcbiAgICAgIHN0eWxlc0luRG9tLnB1c2goe1xuICAgICAgICBpZGVudGlmaWVyOiBpZGVudGlmaWVyLFxuICAgICAgICB1cGRhdGVyOiBhZGRTdHlsZShvYmosIG9wdGlvbnMpLFxuICAgICAgICByZWZlcmVuY2VzOiAxXG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBpZGVudGlmaWVycy5wdXNoKGlkZW50aWZpZXIpO1xuICB9XG5cbiAgcmV0dXJuIGlkZW50aWZpZXJzO1xufVxuXG5mdW5jdGlvbiBpbnNlcnRTdHlsZUVsZW1lbnQob3B0aW9ucykge1xuICB2YXIgc3R5bGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzdHlsZScpO1xuICB2YXIgYXR0cmlidXRlcyA9IG9wdGlvbnMuYXR0cmlidXRlcyB8fCB7fTtcblxuICBpZiAodHlwZW9mIGF0dHJpYnV0ZXMubm9uY2UgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgdmFyIG5vbmNlID0gdHlwZW9mIF9fd2VicGFja19ub25jZV9fICE9PSAndW5kZWZpbmVkJyA/IF9fd2VicGFja19ub25jZV9fIDogbnVsbDtcblxuICAgIGlmIChub25jZSkge1xuICAgICAgYXR0cmlidXRlcy5ub25jZSA9IG5vbmNlO1xuICAgIH1cbiAgfVxuXG4gIE9iamVjdC5rZXlzKGF0dHJpYnV0ZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgIHN0eWxlLnNldEF0dHJpYnV0ZShrZXksIGF0dHJpYnV0ZXNba2V5XSk7XG4gIH0pO1xuXG4gIGlmICh0eXBlb2Ygb3B0aW9ucy5pbnNlcnQgPT09ICdmdW5jdGlvbicpIHtcbiAgICBvcHRpb25zLmluc2VydChzdHlsZSk7XG4gIH0gZWxzZSB7XG4gICAgdmFyIHRhcmdldCA9IGdldFRhcmdldChvcHRpb25zLmluc2VydCB8fCAnaGVhZCcpO1xuXG4gICAgaWYgKCF0YXJnZXQpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihcIkNvdWxkbid0IGZpbmQgYSBzdHlsZSB0YXJnZXQuIFRoaXMgcHJvYmFibHkgbWVhbnMgdGhhdCB0aGUgdmFsdWUgZm9yIHRoZSAnaW5zZXJ0JyBwYXJhbWV0ZXIgaXMgaW52YWxpZC5cIik7XG4gICAgfVxuXG4gICAgdGFyZ2V0LmFwcGVuZENoaWxkKHN0eWxlKTtcbiAgfVxuXG4gIHJldHVybiBzdHlsZTtcbn1cblxuZnVuY3Rpb24gcmVtb3ZlU3R5bGVFbGVtZW50KHN0eWxlKSB7XG4gIC8vIGlzdGFuYnVsIGlnbm9yZSBpZlxuICBpZiAoc3R5bGUucGFyZW50Tm9kZSA9PT0gbnVsbCkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIHN0eWxlLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc3R5bGUpO1xufVxuLyogaXN0YW5idWwgaWdub3JlIG5leHQgICovXG5cblxudmFyIHJlcGxhY2VUZXh0ID0gZnVuY3Rpb24gcmVwbGFjZVRleHQoKSB7XG4gIHZhciB0ZXh0U3RvcmUgPSBbXTtcbiAgcmV0dXJuIGZ1bmN0aW9uIHJlcGxhY2UoaW5kZXgsIHJlcGxhY2VtZW50KSB7XG4gICAgdGV4dFN0b3JlW2luZGV4XSA9IHJlcGxhY2VtZW50O1xuICAgIHJldHVybiB0ZXh0U3RvcmUuZmlsdGVyKEJvb2xlYW4pLmpvaW4oJ1xcbicpO1xuICB9O1xufSgpO1xuXG5mdW5jdGlvbiBhcHBseVRvU2luZ2xldG9uVGFnKHN0eWxlLCBpbmRleCwgcmVtb3ZlLCBvYmopIHtcbiAgdmFyIGNzcyA9IHJlbW92ZSA/ICcnIDogb2JqLm1lZGlhID8gXCJAbWVkaWEgXCIuY29uY2F0KG9iai5tZWRpYSwgXCIge1wiKS5jb25jYXQob2JqLmNzcywgXCJ9XCIpIDogb2JqLmNzczsgLy8gRm9yIG9sZCBJRVxuXG4gIC8qIGlzdGFuYnVsIGlnbm9yZSBpZiAgKi9cblxuICBpZiAoc3R5bGUuc3R5bGVTaGVldCkge1xuICAgIHN0eWxlLnN0eWxlU2hlZXQuY3NzVGV4dCA9IHJlcGxhY2VUZXh0KGluZGV4LCBjc3MpO1xuICB9IGVsc2Uge1xuICAgIHZhciBjc3NOb2RlID0gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoY3NzKTtcbiAgICB2YXIgY2hpbGROb2RlcyA9IHN0eWxlLmNoaWxkTm9kZXM7XG5cbiAgICBpZiAoY2hpbGROb2Rlc1tpbmRleF0pIHtcbiAgICAgIHN0eWxlLnJlbW92ZUNoaWxkKGNoaWxkTm9kZXNbaW5kZXhdKTtcbiAgICB9XG5cbiAgICBpZiAoY2hpbGROb2Rlcy5sZW5ndGgpIHtcbiAgICAgIHN0eWxlLmluc2VydEJlZm9yZShjc3NOb2RlLCBjaGlsZE5vZGVzW2luZGV4XSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHN0eWxlLmFwcGVuZENoaWxkKGNzc05vZGUpO1xuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBhcHBseVRvVGFnKHN0eWxlLCBvcHRpb25zLCBvYmopIHtcbiAgdmFyIGNzcyA9IG9iai5jc3M7XG4gIHZhciBtZWRpYSA9IG9iai5tZWRpYTtcbiAgdmFyIHNvdXJjZU1hcCA9IG9iai5zb3VyY2VNYXA7XG5cbiAgaWYgKG1lZGlhKSB7XG4gICAgc3R5bGUuc2V0QXR0cmlidXRlKCdtZWRpYScsIG1lZGlhKTtcbiAgfSBlbHNlIHtcbiAgICBzdHlsZS5yZW1vdmVBdHRyaWJ1dGUoJ21lZGlhJyk7XG4gIH1cblxuICBpZiAoc291cmNlTWFwICYmIGJ0b2EpIHtcbiAgICBjc3MgKz0gXCJcXG4vKiMgc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247YmFzZTY0LFwiLmNvbmNhdChidG9hKHVuZXNjYXBlKGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShzb3VyY2VNYXApKSkpLCBcIiAqL1wiKTtcbiAgfSAvLyBGb3Igb2xkIElFXG5cbiAgLyogaXN0YW5idWwgaWdub3JlIGlmICAqL1xuXG5cbiAgaWYgKHN0eWxlLnN0eWxlU2hlZXQpIHtcbiAgICBzdHlsZS5zdHlsZVNoZWV0LmNzc1RleHQgPSBjc3M7XG4gIH0gZWxzZSB7XG4gICAgd2hpbGUgKHN0eWxlLmZpcnN0Q2hpbGQpIHtcbiAgICAgIHN0eWxlLnJlbW92ZUNoaWxkKHN0eWxlLmZpcnN0Q2hpbGQpO1xuICAgIH1cblxuICAgIHN0eWxlLmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGNzcykpO1xuICB9XG59XG5cbnZhciBzaW5nbGV0b24gPSBudWxsO1xudmFyIHNpbmdsZXRvbkNvdW50ZXIgPSAwO1xuXG5mdW5jdGlvbiBhZGRTdHlsZShvYmosIG9wdGlvbnMpIHtcbiAgdmFyIHN0eWxlO1xuICB2YXIgdXBkYXRlO1xuICB2YXIgcmVtb3ZlO1xuXG4gIGlmIChvcHRpb25zLnNpbmdsZXRvbikge1xuICAgIHZhciBzdHlsZUluZGV4ID0gc2luZ2xldG9uQ291bnRlcisrO1xuICAgIHN0eWxlID0gc2luZ2xldG9uIHx8IChzaW5nbGV0b24gPSBpbnNlcnRTdHlsZUVsZW1lbnQob3B0aW9ucykpO1xuICAgIHVwZGF0ZSA9IGFwcGx5VG9TaW5nbGV0b25UYWcuYmluZChudWxsLCBzdHlsZSwgc3R5bGVJbmRleCwgZmFsc2UpO1xuICAgIHJlbW92ZSA9IGFwcGx5VG9TaW5nbGV0b25UYWcuYmluZChudWxsLCBzdHlsZSwgc3R5bGVJbmRleCwgdHJ1ZSk7XG4gIH0gZWxzZSB7XG4gICAgc3R5bGUgPSBpbnNlcnRTdHlsZUVsZW1lbnQob3B0aW9ucyk7XG4gICAgdXBkYXRlID0gYXBwbHlUb1RhZy5iaW5kKG51bGwsIHN0eWxlLCBvcHRpb25zKTtcblxuICAgIHJlbW92ZSA9IGZ1bmN0aW9uIHJlbW92ZSgpIHtcbiAgICAgIHJlbW92ZVN0eWxlRWxlbWVudChzdHlsZSk7XG4gICAgfTtcbiAgfVxuXG4gIHVwZGF0ZShvYmopO1xuICByZXR1cm4gZnVuY3Rpb24gdXBkYXRlU3R5bGUobmV3T2JqKSB7XG4gICAgaWYgKG5ld09iaikge1xuICAgICAgaWYgKG5ld09iai5jc3MgPT09IG9iai5jc3MgJiYgbmV3T2JqLm1lZGlhID09PSBvYmoubWVkaWEgJiYgbmV3T2JqLnNvdXJjZU1hcCA9PT0gb2JqLnNvdXJjZU1hcCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHVwZGF0ZShvYmogPSBuZXdPYmopO1xuICAgIH0gZWxzZSB7XG4gICAgICByZW1vdmUoKTtcbiAgICB9XG4gIH07XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGxpc3QsIG9wdGlvbnMpIHtcbiAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307IC8vIEZvcmNlIHNpbmdsZS10YWcgc29sdXRpb24gb24gSUU2LTksIHdoaWNoIGhhcyBhIGhhcmQgbGltaXQgb24gdGhlICMgb2YgPHN0eWxlPlxuICAvLyB0YWdzIGl0IHdpbGwgYWxsb3cgb24gYSBwYWdlXG5cbiAgaWYgKCFvcHRpb25zLnNpbmdsZXRvbiAmJiB0eXBlb2Ygb3B0aW9ucy5zaW5nbGV0b24gIT09ICdib29sZWFuJykge1xuICAgIG9wdGlvbnMuc2luZ2xldG9uID0gaXNPbGRJRSgpO1xuICB9XG5cbiAgbGlzdCA9IGxpc3QgfHwgW107XG4gIHZhciBsYXN0SWRlbnRpZmllcnMgPSBtb2R1bGVzVG9Eb20obGlzdCwgb3B0aW9ucyk7XG4gIHJldHVybiBmdW5jdGlvbiB1cGRhdGUobmV3TGlzdCkge1xuICAgIG5ld0xpc3QgPSBuZXdMaXN0IHx8IFtdO1xuXG4gICAgaWYgKE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChuZXdMaXN0KSAhPT0gJ1tvYmplY3QgQXJyYXldJykge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGFzdElkZW50aWZpZXJzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgaWRlbnRpZmllciA9IGxhc3RJZGVudGlmaWVyc1tpXTtcbiAgICAgIHZhciBpbmRleCA9IGdldEluZGV4QnlJZGVudGlmaWVyKGlkZW50aWZpZXIpO1xuICAgICAgc3R5bGVzSW5Eb21baW5kZXhdLnJlZmVyZW5jZXMtLTtcbiAgICB9XG5cbiAgICB2YXIgbmV3TGFzdElkZW50aWZpZXJzID0gbW9kdWxlc1RvRG9tKG5ld0xpc3QsIG9wdGlvbnMpO1xuXG4gICAgZm9yICh2YXIgX2kgPSAwOyBfaSA8IGxhc3RJZGVudGlmaWVycy5sZW5ndGg7IF9pKyspIHtcbiAgICAgIHZhciBfaWRlbnRpZmllciA9IGxhc3RJZGVudGlmaWVyc1tfaV07XG5cbiAgICAgIHZhciBfaW5kZXggPSBnZXRJbmRleEJ5SWRlbnRpZmllcihfaWRlbnRpZmllcik7XG5cbiAgICAgIGlmIChzdHlsZXNJbkRvbVtfaW5kZXhdLnJlZmVyZW5jZXMgPT09IDApIHtcbiAgICAgICAgc3R5bGVzSW5Eb21bX2luZGV4XS51cGRhdGVyKCk7XG5cbiAgICAgICAgc3R5bGVzSW5Eb20uc3BsaWNlKF9pbmRleCwgMSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgbGFzdElkZW50aWZpZXJzID0gbmV3TGFzdElkZW50aWZpZXJzO1xuICB9O1xufTsiLCJleHBvcnQgZGVmYXVsdCBfX3dlYnBhY2tfcHVibGljX3BhdGhfXyArIFwiMTk5YjExNWJkNjI3OGE5NTA5NTkwMTgyMjkxZGFkZTUuc3ZnXCI7IiwiZXhwb3J0IGRlZmF1bHQgX193ZWJwYWNrX3B1YmxpY19wYXRoX18gKyBcIjJkN2FlYjQyNGY0MTk3NDQ3ZjlkYzNjMDc2Nzg1MzMzLnN2Z1wiOyIsImV4cG9ydCBkZWZhdWx0IF9fd2VicGFja19wdWJsaWNfcGF0aF9fICsgXCJmMjJlM2IzMWMxZDc0MzMxOTk0YjhkMjk0MDQ3OWVkMy5zdmdcIjsiLCJleHBvcnQgZGVmYXVsdCBfX3dlYnBhY2tfcHVibGljX3BhdGhfXyArIFwiNWQ0ZGVhNGFmMDllMmRiYTBjZDBiMWNiMjc2ZDc2ZTAuc3ZnXCI7IiwiZXhwb3J0IGRlZmF1bHQgX193ZWJwYWNrX3B1YmxpY19wYXRoX18gKyBcImNhODlmMzFhNzUzMjMxODZjZDYwMjBhMGJjZGQ5YmEyLnN2Z1wiOyIsImV4cG9ydCBkZWZhdWx0IF9fd2VicGFja19wdWJsaWNfcGF0aF9fICsgXCI3NjllOWRlNjA2YWVlNWQ3MzU3M2M1N2ZjYjVjZDdjYi5zdmdcIjsiLCJleHBvcnQgZGVmYXVsdCBfX3dlYnBhY2tfcHVibGljX3BhdGhfXyArIFwiMTllMGNhNzRiM2E2NzBhYWUyNTRiNmRmMTc5OTIyNDYuc3ZnXCI7IiwiZXhwb3J0IGRlZmF1bHQgX193ZWJwYWNrX3B1YmxpY19wYXRoX18gKyBcIjViOTNjNjBlN2I0MTQyZjQxZGZkOGU5NWYwNWJkZDRiLnN2Z1wiOyIsImltcG9ydCBpbWdzcmMgZnJvbSAnLi9wcm9maWxlX2ltYWdlLmpwZyc7XG5pbXBvcnQgc3Rhcl9vIGZyb20gJy4uLy4uL2Fzc2V0cy9zdGFyLW91dGxpbmUuc3ZnJztcbmltcG9ydCBzdGFyIGZyb20gJy4uLy4uL2Fzc2V0cy9zdGFyLnN2Zyc7XG5pbXBvcnQgYWRkIGZyb20gJy4uLy4uL2Fzc2V0cy9hZGQtY2lyY2xlLnN2Zyc7XG5pbXBvcnQgY2FsbCBmcm9tICcuLi8uLi9hc3NldHMvY2FsbC1vdXRsaW5lLnN2Zyc7XG5pbXBvcnQgbG9jIGZyb20gJy4uLy4uL2Fzc2V0cy9sb2NhdGlvbi1vdXRsaW5lLnN2Zyc7XG5cbmltcG9ydCB7IGdldENvbXBvbmVudCBhcyBnZXROYXZDb21wb25lbnQgfSBmcm9tICcuL21lbnUvaW5kZXguanMnO1xuXG5mdW5jdGlvbiBnZXRDb21wb25lbnQoKSB7XG4gIC8vIGNyZWF0ZSBoZWFkZXJcbiAgbGV0IGhlYWRlciA9IGNyZWF0ZUVsZW1lbnQoJ2hlYWRlcicsICdjLWhlYWRlcicpO1xuXG4gIC8vIGNyZWF0ZSBsYXlvdXRfMVxuICBsZXQgbGF5b3V0XzEgPSBjcmVhdGVFbGVtZW50KCdkaXYnLCAnYy1sYXlvdXQnKTtcblxuICAvLyBjcmVhdGUgbGF5b3V0XzJcbiAgbGV0IGxheW91dF8yID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2MtbGF5b3V0Jyk7XG5cbiAgLy8gY3JlYXRlIGxheW91dF8zXG4gIGxldCBsYXlvdXRfMyA9IGNyZWF0ZUVsZW1lbnQoJ2RpdicsICdjLWxheW91dCcpO1xuXG4gIGxldCBidG5fd3JhcHBlciA9IGNyZWF0ZUVsZW1lbnQoJ2RpdicsICdjLWJ1dHRvbl9fd3JhcHBlcicpO1xuXG4gIC8vY3JlYXRlIGJ1dHRvblxuICBsZXQgYnV0dG9uID0gY3JlYXRlRWxlbWVudCgnYnV0dG9uJywgWydjLWJ1dHRvbicsICdjLWJ1dHRvbi0tcHJpbWFyeSddKTtcbiAgYnV0dG9uLmlubmVySFRNTCA9ICdMb2dvdXQnO1xuXG4gIC8vIGNyZWF0ZSAyNSUgZGVza3RvcFxuICBsZXQgZF8yNSA9IGNyZWF0ZUVsZW1lbnQoJ2RpdicsICdjLWNvbnRhaW5lcl9fMjUtZCcpO1xuICBsZXQgZF8yNV9jb24gPSBjcmVhdGVFbGVtZW50KCdkaXYnLCAnYy1jb250YWluZXInKTtcblxuICAvLyBjcmVhdGUgNzUlIGRlc2t0b3BcbiAgbGV0IGRfNzUgPSBjcmVhdGVFbGVtZW50KCdkaXYnLCAnYy1jb250YWluZXJfXzc1LWQnKTtcbiAgbGV0IGRfNzVfY29uID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2MtY29udGFpbmVyJyk7XG5cbiAgLy8gY3JlYXRlIGltYWdlXG4gIGxldCBpbWFnZSA9IGNyZWF0ZUVsZW1lbnQoJ2ltZycpO1xuICBpbWFnZS5zcmMgPSBpbWdzcmM7XG5cbiAgLy8gY3JlYXRlIGluZm9zXG4gIGxldCBpbmZvXzEgPSBjcmVhdGVFbGVtZW50KCdkaXYnLCAnYy1pbmZvX19saXN0Jyk7XG4gIGluZm9fMS5pbm5lckhUTUwgPSAnSmVzc2ljYSBQYXJrZXInO1xuXG4gIGxldCBpbmZvXzIgPSBjcmVhdGVFbGVtZW50KCdkaXYnLCAnYy1pbmZvX19saXN0Jyk7XG4gIGxldCBpbmZvXzMgPSBjcmVhdGVFbGVtZW50KCdkaXYnLCAnYy1pbmZvX19saXN0Jyk7XG5cbiAgbGV0IGluZm9fNCA9IGNyZWF0ZUVsZW1lbnQoJ2RpdicsICdjLWluZm9fX2xpc3QnKTtcblxuICBsZXQgaW5mb181ID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2MtaW5mb19fbGlzdCcpO1xuICBpbmZvXzUuaW5uZXJIVE1MID0gJzYgUmV2aWV3cyc7XG5cbiAgLy8gc3VtbW9uIGljb25zXG4gIC8vIHN0YXJcbiAgbGV0IGljb25fc18xID0gY3JlYXRlRWxlbWVudCgnaW1nJywgJ2MtaWNvbicpO1xuICBpY29uX3NfMS5zcmMgPSBzdGFyO1xuICBsZXQgaWNvbl9zXzIgPSBjcmVhdGVFbGVtZW50KCdpbWcnLCAnYy1pY29uJyk7XG4gIGljb25fc18yLnNyYyA9IHN0YXI7XG4gIGxldCBpY29uX3NfMyA9IGNyZWF0ZUVsZW1lbnQoJ2ltZycsICdjLWljb24nKTtcbiAgaWNvbl9zXzMuc3JjID0gc3RhcjtcbiAgbGV0IGljb25fc180ID0gY3JlYXRlRWxlbWVudCgnaW1nJywgJ2MtaWNvbicpO1xuICBpY29uX3NfNC5zcmMgPSBzdGFyO1xuICAvLyBzdGFyLW91dGxpbmVcbiAgbGV0IGljb25fc28gPSBjcmVhdGVFbGVtZW50KCdpbWcnLCAnYy1pY29uJyk7XG4gIGljb25fc28uc3JjID0gc3Rhcl9vO1xuICAvLyBhZGQtY2lyY2xlXG4gIGxldCBpY29uX2EgPSBjcmVhdGVFbGVtZW50KCdpbWcnLCAnYy1pY29uJyk7XG4gIGljb25fYS5zcmMgPSBhZGQ7XG4gIC8vIGxvY2F0aW9uXG4gIGxldCBpY29uX2wgPSBjcmVhdGVFbGVtZW50KCdpbWcnLCAnYy1pY29uJyk7XG4gIGljb25fbC5zcmMgPSBsb2M7XG4gIC8vIGNhbGxcbiAgbGV0IGljb25fcCA9IGNyZWF0ZUVsZW1lbnQoJ2ltZycsICdjLWljb24nKTtcbiAgaWNvbl9wLnNyYyA9IGNhbGw7XG5cbiAgbGV0IGluZm9fNiA9IGNyZWF0ZUVsZW1lbnQoJ2RpdicsICdjLWluZm9fX2xpc3QnKTtcblxuICAvLyBjcmVhdGUgZGl2aWRlclxuICBsZXQgY29sX184ID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2MtY29sX183Jyk7XG4gIGxldCBjb2xfXzQgPSBjcmVhdGVFbGVtZW50KCdkaXYnLCAnYy1jb2xfXzUnKTtcblxuICAvLyBjcmVhdGUgaHJcbiAgbGV0IGhyID0gY3JlYXRlRWxlbWVudCgnaHInKTtcblxuICAvLyBBUFBFTkRTXG4gIC8vIGFwcGVuZCBpY29uc1xuICBpbmZvXzQuYXBwZW5kQ2hpbGQoaWNvbl9zXzEpO1xuICBpbmZvXzQuYXBwZW5kQ2hpbGQoaWNvbl9zXzIpO1xuICBpbmZvXzQuYXBwZW5kQ2hpbGQoaWNvbl9zXzMpO1xuICBpbmZvXzQuYXBwZW5kQ2hpbGQoaWNvbl9zXzQpO1xuICBpbmZvXzQuYXBwZW5kQ2hpbGQoaWNvbl9zbyk7XG4gIGluZm9fMi5hcHBlbmRDaGlsZChpY29uX2wpO1xuICBpbmZvXzMuYXBwZW5kQ2hpbGQoaWNvbl9wKTtcblxuICAvLyBhcHBlbmQgYnV0dG9uXG4gIGJ0bl93cmFwcGVyLmFwcGVuZENoaWxkKGJ1dHRvbik7XG4gIGxheW91dF8xLmFwcGVuZENoaWxkKGJ0bl93cmFwcGVyKTtcblxuICAvLyBhcHBlbmQgaW1hZ2VcbiAgZF8yNV9jb24uYXBwZW5kQ2hpbGQoaW1hZ2UpO1xuICBkXzI1LmFwcGVuZENoaWxkKGRfMjVfY29uKTtcblxuICAvLyBhcHBlbmQgaW5mb1xuICBpbmZvXzIuaW5uZXJIVE1MICs9ICdOZXdwb3J0IEJlYWNoLCBDQSc7XG4gIGluZm9fMy5pbm5lckhUTUwgKz0gJyg5NDkpIDMyNS02ODU5NCc7XG5cbiAgY29sX184LmFwcGVuZENoaWxkKGluZm9fMSk7XG4gIGNvbF9fOC5hcHBlbmRDaGlsZChpbmZvXzIpO1xuICBjb2xfXzguYXBwZW5kQ2hpbGQoaW5mb18zKTtcbiAgZF83NV9jb24uYXBwZW5kQ2hpbGQoY29sX184KTtcblxuICBjb2xfXzQuYXBwZW5kQ2hpbGQoaW5mb180KTtcbiAgY29sX180LmFwcGVuZENoaWxkKGluZm9fNSk7XG4gIGRfNzVfY29uLmFwcGVuZENoaWxkKGNvbF9fNCk7XG5cbiAgLy8gYXBwZW5kIDc1IHRvIGNvbnRhaW5lclxuICBkXzc1LmFwcGVuZENoaWxkKGRfNzVfY29uKTtcblxuICAvLyBhcHBlbmQgZm9sbG93ZXJzIHRvIGNvbnRhaW5lclxuICBpbmZvXzYuYXBwZW5kQ2hpbGQoaWNvbl9hKTtcbiAgaW5mb182LmlubmVySFRNTCArPSAnMTUgRm9sbG93ZXJzJztcbiAgZF83NS5hcHBlbmRDaGlsZChpbmZvXzYpO1xuXG4gIC8vIGFwcGVuZCBuYXZcbiAgZF83NS5hcHBlbmRDaGlsZChnZXROYXZDb21wb25lbnQoKSk7XG5cbiAgLy8gYXBwZW5kIGNvbnRhaW5lcnNcbiAgbGF5b3V0XzMuYXBwZW5kQ2hpbGQoZF8yNSk7XG4gIGxheW91dF8zLmFwcGVuZENoaWxkKGRfNzUpO1xuXG4gIC8vIGFwcGVuZCBsYXlvdXQtMVxuICBoZWFkZXIuYXBwZW5kQ2hpbGQobGF5b3V0XzEpO1xuXG4gIC8vIGFwcGVuZCBsYXlvdXQtMlxuICBoZWFkZXIuYXBwZW5kQ2hpbGQobGF5b3V0XzIpO1xuXG4gIC8vIGFwcGVuZCBoclxuICBoZWFkZXIuYXBwZW5kQ2hpbGQoaHIpO1xuXG4gIC8vIGFwcGVuZCBsYXlvdXQtM1xuICBoZWFkZXIuYXBwZW5kQ2hpbGQobGF5b3V0XzMpO1xuXG4gIHJldHVybiBoZWFkZXI7XG59XG5cbmZ1bmN0aW9uIGNyZWF0ZUVsZW1lbnQoZWxUYWcsIGVsQ2xhc3MpIHtcbiAgbGV0IGVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChlbFRhZyk7XG5cbiAgaWYgKHR5cGVvZiBlbENsYXNzID09PSAnc3RyaW5nJykge1xuICAgIGVsLmNsYXNzTGlzdC5hZGQoZWxDbGFzcyk7XG4gIH0gZWxzZSBpZiAodHlwZW9mIGVsQ2xhc3MgPT09ICdvYmplY3QnKSB7XG4gICAgaWYgKGVsQ2xhc3MubGVuZ3RoID49IDEpIHtcbiAgICAgIGVsQ2xhc3MuZm9yRWFjaChjID0+IHtcbiAgICAgICAgZWwuY2xhc3NMaXN0LmFkZChjKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBlbDtcbn1cblxuZXhwb3J0IHsgZ2V0Q29tcG9uZW50IH07XG4iLCJmdW5jdGlvbiBnZXRDb21wb25lbnQoKSB7XG4gIC8vIGNyZWF0ZSBuYXZcbiAgbGV0IG5hdiA9IGNyZWF0ZUVsZW1lbnQoJ25hdicsICdjLW5hdicpO1xuXG4gIC8vIGNyZWF0ZSB1bFxuICBsZXQgdWwgPSBjcmVhdGVFbGVtZW50KCd1bCcsICdjLXVsJyk7XG5cbiAgLy8gY3JlYXRlIGxpc3RcbiAgbGV0IGxpXzEgPSBjcmVhdGVFbGVtZW50KCdsaScsIFsnYy1saXN0JywgJ2FjdGl2ZSddKTtcbiAgbGV0IGxpXzIgPSBjcmVhdGVFbGVtZW50KCdsaScsICdjLWxpc3QnKTtcbiAgbGV0IGxpXzMgPSBjcmVhdGVFbGVtZW50KCdsaScsICdjLWxpc3QnKTtcbiAgbGV0IGxpXzQgPSBjcmVhdGVFbGVtZW50KCdsaScsICdjLWxpc3QnKTtcbiAgbGV0IGxpXzUgPSBjcmVhdGVFbGVtZW50KCdsaScsICdjLWxpc3QnKTtcblxuICAvLyBjcmVhdGUgbGlua1xuICBsZXQgYV8xID0gY3JlYXRlRWxlbWVudCgnYScsICdjLWxpbmsnKTtcbiAgYV8xLmlubmVySFRNTCA9ICdBYm91dCc7XG5cbiAgbGV0IGFfMiA9IGNyZWF0ZUVsZW1lbnQoJ2EnLCAnYy1saW5rJyk7XG4gIGFfMi5pbm5lckhUTUwgPSAnU2V0dGluZ3MnO1xuXG4gIGxldCBhXzMgPSBjcmVhdGVFbGVtZW50KCdhJywgJ2MtbGluaycpO1xuICBhXzMuaW5uZXJIVE1MID0gJ09wdGlvbjEnO1xuXG4gIGxldCBhXzQgPSBjcmVhdGVFbGVtZW50KCdhJywgJ2MtbGluaycpO1xuICBhXzQuaW5uZXJIVE1MID0gJ09wdGlvbjInO1xuXG4gIGxldCBhXzUgPSBjcmVhdGVFbGVtZW50KCdhJywgJ2MtbGluaycpO1xuICBhXzUuaW5uZXJIVE1MID0gJ09wdGlvbjMnO1xuXG4gIC8vIEFQUEVORFNcbiAgbGlfMS5hcHBlbmRDaGlsZChhXzEpO1xuICBsaV8yLmFwcGVuZENoaWxkKGFfMik7XG4gIGxpXzMuYXBwZW5kQ2hpbGQoYV8zKTtcbiAgbGlfNC5hcHBlbmRDaGlsZChhXzQpO1xuICBsaV81LmFwcGVuZENoaWxkKGFfNSk7XG5cbiAgdWwuYXBwZW5kQ2hpbGQobGlfMSk7XG4gIHVsLmFwcGVuZENoaWxkKGxpXzIpO1xuICB1bC5hcHBlbmRDaGlsZChsaV8zKTtcbiAgdWwuYXBwZW5kQ2hpbGQobGlfNCk7XG4gIHVsLmFwcGVuZENoaWxkKGxpXzUpO1xuXG4gIG5hdi5hcHBlbmRDaGlsZCh1bCk7XG5cbiAgcmV0dXJuIG5hdjtcbn1cblxuZnVuY3Rpb24gY3JlYXRlRWxlbWVudChlbFRhZywgZWxDbGFzcykge1xuICAvLyBjcmVhdGUgZWxlbWVudFxuICBsZXQgZWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KGVsVGFnKTtcblxuICAvLyBhZGRzIGNsYXNzXG4gIGlmICh0eXBlb2YgZWxDbGFzcyA9PT0gJ3N0cmluZycpIHtcbiAgICBlbC5jbGFzc0xpc3QuYWRkKGVsQ2xhc3MpO1xuICB9IGVsc2UgaWYgKHR5cGVvZiBlbENsYXNzID09PSAnb2JqZWN0Jykge1xuICAgIGlmIChlbENsYXNzLmxlbmd0aCA+PSAxKSB7XG4gICAgICBlbENsYXNzLmZvckVhY2goYyA9PiB7XG4gICAgICAgIGVsLmNsYXNzTGlzdC5hZGQoYyk7XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gZWw7XG59XG5cbi8vIGNvbnN0IF9nZXRDb21wb25lbnQgPSBnZXRDb21wb25lbnQ7XG5leHBvcnQgeyBnZXRDb21wb25lbnQgfTtcbiIsImV4cG9ydCBkZWZhdWx0IF9fd2VicGFja19wdWJsaWNfcGF0aF9fICsgXCIwNDAzNGNjNTJlMGM3ODRlMzE4Y2NjNjlkODcyNzNhYS5qcGdcIjsiLCJmdW5jdGlvbiBnZXRDb21wb25lbnQobmFtZSwgdmFsdWUpIHtcbiAgbGV0IGdyb3VwID0gY3JlYXRlRWxlbWVudCgnZGl2JywgWydjLWlucHV0X19ncm91cCcsICdjLWlucHV0LS1pcy1maWxsZWQnXSk7XG5cbiAgbGV0IGxhYmVsID0gY3JlYXRlRWxlbWVudCgnbGFiZWwnLCAnYy1pbnB1dF9fbGFiZWwnKTtcbiAgbGFiZWwuc2V0QXR0cmlidXRlKCdmb3InLCBuYW1lKTtcbiAgbGFiZWwuaW5uZXJIVE1MID0gbGFiZWxpemUobmFtZSk7XG5cbiAgbGV0IGZpZWxkID0gY3JlYXRlRWxlbWVudCgnaW5wdXQnLCAnYy1pbnB1dF9fZmllbGQnKTtcbiAgZmllbGQuc2V0QXR0cmlidXRlKCd0eXBlJywgJ3RleHQnKTtcbiAgZmllbGQuc2V0QXR0cmlidXRlKCduYW1lJywgbmFtZSk7XG4gIGZpZWxkLnNldEF0dHJpYnV0ZSgndmFsdWUnLCB2YWx1ZSk7XG5cbiAgLy8gQVBQRU5EXG4gIGdyb3VwLmFwcGVuZENoaWxkKGxhYmVsKTtcbiAgZ3JvdXAuYXBwZW5kQ2hpbGQoZmllbGQpO1xuXG4gIHJldHVybiBncm91cDtcbn1cblxuZnVuY3Rpb24gY3JlYXRlRWxlbWVudChlbFRhZywgZWxDbGFzcykge1xuICAvLyBjcmVhdGUgZWxlbWVudFxuICBsZXQgZWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KGVsVGFnKTtcblxuICAvLyBhZGRzIGNsYXNzXG4gIGlmICh0eXBlb2YgZWxDbGFzcyA9PT0gJ3N0cmluZycpIHtcbiAgICBlbC5jbGFzc0xpc3QuYWRkKGVsQ2xhc3MpO1xuICB9IGVsc2UgaWYgKHR5cGVvZiBlbENsYXNzID09PSAnb2JqZWN0Jykge1xuICAgIGlmIChlbENsYXNzLmxlbmd0aCA+PSAxKSB7XG4gICAgICBlbENsYXNzLmZvckVhY2goYyA9PiB7XG4gICAgICAgIGVsLmNsYXNzTGlzdC5hZGQoYyk7XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gZWw7XG59XG5cbmZ1bmN0aW9uIGdldEZpcnN0TmFtZSgpIHtcbiAgcmV0dXJuIGdldENvbXBvbmVudCgnZmlyc3RfbmFtZScsICdKZXNzaWNhJyk7XG59XG5cbmZ1bmN0aW9uIGdldExhc3ROYW1lKCkge1xuICByZXR1cm4gZ2V0Q29tcG9uZW50KCdsYXN0X25hbWUnLCAnUGFya2VyJyk7XG59XG5cbmZ1bmN0aW9uIGdldExvY2F0aW9uKCkge1xuICByZXR1cm4gZ2V0Q29tcG9uZW50KCdsb2NhdGlvbicsICdOZXdwb3J0IEJlYWNoLCBDQScpO1xufVxuXG5mdW5jdGlvbiBnZXRQaG9uZSgpIHtcbiAgcmV0dXJuIGdldENvbXBvbmVudCgncGhvbmUnLCAnKDk0OSkgMzI1LTY4NTk0Jyk7XG59XG5cbmZ1bmN0aW9uIGdldFdlYnNpdGUoKSB7XG4gIHJldHVybiBnZXRDb21wb25lbnQoJ3dlYnNpdGUnLCAnd3d3LnNlbGxlci5jb20nKTtcbn1cblxuZnVuY3Rpb24gbGFiZWxpemUobmFtZSkge1xuICByZXR1cm4gbmFtZS5yZXBsYWNlKCdfJywgJyAnKS50b1VwcGVyQ2FzZSgpO1xufVxuXG5leHBvcnQgeyBnZXRGaXJzdE5hbWUsIGdldExhc3ROYW1lLCBnZXRMb2NhdGlvbiwgZ2V0UGhvbmUsIGdldFdlYnNpdGUgfTtcbiIsImltcG9ydCB7XG4gIGdldEZpcnN0TmFtZSxcbiAgZ2V0TGFzdE5hbWUsXG4gIGdldExvY2F0aW9uLFxuICBnZXRQaG9uZSxcbiAgZ2V0V2Vic2l0ZVxufSBmcm9tICcuL2dyb3VwL2luZGV4LmpzJztcblxuZnVuY3Rpb24gZ2V0Q29tcG9uZW50KCkge1xuICBsZXQgZm9ybSA9IGNyZWF0ZUVsZW1lbnQoJ2Zvcm0nLCBbJ2MtZm9ybScsICdjLWFsdGVyX19mb3JtJ10pO1xuXG4gIGZvcm0uYXBwZW5kQ2hpbGQoZ2V0Rmlyc3ROYW1lKCkpO1xuICBmb3JtLmFwcGVuZENoaWxkKGdldExhc3ROYW1lKCkpO1xuICBmb3JtLmFwcGVuZENoaWxkKGdldExvY2F0aW9uKCkpO1xuICBmb3JtLmFwcGVuZENoaWxkKGdldFBob25lKCkpO1xuICBmb3JtLmFwcGVuZENoaWxkKGdldFdlYnNpdGUoKSk7XG5cbiAgcmV0dXJuIGZvcm07XG59XG5cbmZ1bmN0aW9uIGNyZWF0ZUVsZW1lbnQoZWxUYWcsIGVsQ2xhc3MpIHtcbiAgLy8gY3JlYXRlIGVsZW1lbnRcbiAgbGV0IGVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChlbFRhZyk7XG5cbiAgLy8gYWRkcyBjbGFzc1xuICBpZiAodHlwZW9mIGVsQ2xhc3MgPT09ICdzdHJpbmcnKSB7XG4gICAgZWwuY2xhc3NMaXN0LmFkZChlbENsYXNzKTtcbiAgfSBlbHNlIGlmICh0eXBlb2YgZWxDbGFzcyA9PT0gJ29iamVjdCcpIHtcbiAgICBpZiAoZWxDbGFzcy5sZW5ndGggPj0gMSkge1xuICAgICAgZWxDbGFzcy5mb3JFYWNoKGMgPT4ge1xuICAgICAgICBlbC5jbGFzc0xpc3QuYWRkKGMpO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIGVsO1xufVxuXG5leHBvcnQgeyBnZXRDb21wb25lbnQgfTtcbiIsImltcG9ydCBnbG9iZSBmcm9tICcuLi8uLi9hc3NldHMvZ2xvYmUtb3V0bGluZS5zdmcnO1xuaW1wb3J0IGNhbGwgZnJvbSAnLi4vLi4vYXNzZXRzL2NhbGwtb3V0bGluZS5zdmcnO1xuaW1wb3J0IGhvbWUgZnJvbSAnLi4vLi4vYXNzZXRzL2hvbWUtb3V0bGluZS5zdmcnO1xuaW1wb3J0IHBlbmNpbCBmcm9tICcuLi8uLi9hc3NldHMvcGVuY2lsLW91dGxpbmUuc3ZnJztcbmltcG9ydCB7IGdldENvbXBvbmVudCBhcyBnZXRGb3JtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtL2luZGV4LmpzJztcblxuZnVuY3Rpb24gZ2V0Q29tcG9uZW50KCkge1xuICAvLyBjcmVhdGUgbWFpblxuICBsZXQgbWFpbiA9IGNyZWF0ZUVsZW1lbnQoJ21haW4nLCAnYy1tYWluJyk7XG5cbiAgLy8gY3JlYXRlIGxheW91dHNcbiAgbGV0IGxheW91dF8xID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2MtbGF5b3V0Jyk7XG4gIGxldCBsYXlvdXRfMiA9IGNyZWF0ZUVsZW1lbnQoJ2RpdicsIFsnYy1sYXlvdXQnLCAnYy1hbHRlcl9fZGlzcGxheSddKTtcblxuICAvLyBjcmVhdGUgaDNcbiAgbGV0IGgzID0gY3JlYXRlRWxlbWVudCgnaDMnKTtcbiAgaDMuaW5uZXJIVE1MID0gJ0Fib3V0JztcblxuICAvLyBjcmVhdGUgYnV0dG9uXG4gIGxldCBidXR0b24gPSBjcmVhdGVFbGVtZW50KCdidXR0b24nLCAnYy1idG5fX2VkaXQnKTtcbiAgbGV0IGJ0bl9jYW5jZWwgPSBjcmVhdGVFbGVtZW50KCdidXR0b24nLCAnYy1idG5fX2NhbmNlbCcpO1xuICBidG5fY2FuY2VsLmlubmVySFRNTCA9ICdDQU5DRUwnO1xuICBsZXQgYnRuX3NhdmUgPSBjcmVhdGVFbGVtZW50KCdidXR0b24nLCAnYy1idG5fX3NhdmUnKTtcbiAgYnRuX3NhdmUuaW5uZXJIVE1MID0gJ1NBVkUnO1xuXG4gIC8vIGNyZWF0ZSBpdGVtc1xuICBsZXQgaXRlbV8xID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2MtaXRlbScpO1xuICBsZXQgaXRlbV8yID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2MtaXRlbScpO1xuICBsZXQgaXRlbV8zID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2MtaXRlbScpO1xuICBsZXQgaXRlbV80ID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2MtaXRlbScpO1xuXG4gIGxldCBpdGVtXzFfc3BhbiA9IGNyZWF0ZUVsZW1lbnQoJ3NwYW4nKTtcbiAgbGV0IGl0ZW1fMl9zcGFuID0gY3JlYXRlRWxlbWVudCgnc3BhbicpO1xuICBsZXQgaXRlbV8zX3NwYW4gPSBjcmVhdGVFbGVtZW50KCdzcGFuJyk7XG4gIGxldCBpdGVtXzRfc3BhbiA9IGNyZWF0ZUVsZW1lbnQoJ3NwYW4nKTtcblxuICBpdGVtXzFfc3Bhbi5pbm5lckhUTUwgPSAnSmVzc2ljYSBQYXJrZXInO1xuICBpdGVtXzJfc3Bhbi5pbm5lckhUTUwgPSAnd3d3LnNlbGxlci5jb20nO1xuICBpdGVtXzNfc3Bhbi5pbm5lckhUTUwgPSAnKDk0OSkgMzI1IC0gNjg1OTQnO1xuICBpdGVtXzRfc3Bhbi5pbm5lckhUTUwgPSAnTmV3cG9ydCBCZWFjaCwgQ0EnO1xuXG4gIGxldCBpdGVtXzFfY29udGFpbmVyID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2MtY29udGFpbmVyJyk7XG4gIGxldCBpdGVtXzJfY29udGFpbmVyID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2MtY29udGFpbmVyJyk7XG4gIGxldCBpdGVtXzNfY29udGFpbmVyID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2MtY29udGFpbmVyJyk7XG4gIGxldCBpdGVtXzRfY29udGFpbmVyID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2MtY29udGFpbmVyJyk7XG5cbiAgbGV0IGl0ZW1fMV9lZGl0ID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2MtZWRpdCcpO1xuICBsZXQgaXRlbV8yX2VkaXQgPSBjcmVhdGVFbGVtZW50KCdkaXYnLCAnYy1lZGl0Jyk7XG4gIGxldCBpdGVtXzNfZWRpdCA9IGNyZWF0ZUVsZW1lbnQoJ2RpdicsICdjLWVkaXQnKTtcbiAgbGV0IGl0ZW1fNF9lZGl0ID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2MtZWRpdCcpO1xuXG4gIC8vIGljb25zXG4gIGxldCBpY29uR2xvYmUgPSBjcmVhdGVFbGVtZW50KCdpbWcnLCAnYy1pY29uJyk7XG4gIGljb25HbG9iZS5zcmMgPSBnbG9iZTtcblxuICBsZXQgaWNvbkNhbGwgPSBjcmVhdGVFbGVtZW50KCdpbWcnLCAnYy1pY29uJyk7XG4gIGljb25DYWxsLnNyYyA9IGNhbGw7XG5cbiAgbGV0IGljb25Ib21lID0gY3JlYXRlRWxlbWVudCgnaW1nJywgJ2MtaWNvbicpO1xuICBpY29uSG9tZS5zcmMgPSBob21lO1xuXG4gIGxldCBpY29uUGVuY2lsXzEgPSBjcmVhdGVFbGVtZW50KCdpbWcnLCAnYy1pY29uJyk7XG4gIGljb25QZW5jaWxfMS5zcmMgPSBwZW5jaWw7XG5cbiAgbGV0IGljb25QZW5jaWxfMiA9IGNyZWF0ZUVsZW1lbnQoJ2ltZycsICdjLWljb24nKTtcbiAgaWNvblBlbmNpbF8yLnNyYyA9IHBlbmNpbDtcblxuICBsZXQgaWNvblBlbmNpbF8zID0gY3JlYXRlRWxlbWVudCgnaW1nJywgJ2MtaWNvbicpO1xuICBpY29uUGVuY2lsXzMuc3JjID0gcGVuY2lsO1xuXG4gIGxldCBpY29uUGVuY2lsXzQgPSBjcmVhdGVFbGVtZW50KCdpbWcnLCAnYy1pY29uJyk7XG4gIGljb25QZW5jaWxfNC5zcmMgPSBwZW5jaWw7XG5cbiAgbGV0IGljb25QZW5jaWxfNSA9IGNyZWF0ZUVsZW1lbnQoJ2ltZycsICdjLWljb24nKTtcbiAgaWNvblBlbmNpbF81LnNyYyA9IHBlbmNpbDtcblxuICAvLyBBUFBFTkRTXG4gIGJ1dHRvbi5hcHBlbmRDaGlsZChpY29uUGVuY2lsXzUpO1xuXG4gIGxheW91dF8xLmFwcGVuZENoaWxkKGgzKTtcbiAgbGF5b3V0XzEuYXBwZW5kQ2hpbGQoYnV0dG9uKTtcbiAgbGF5b3V0XzEuYXBwZW5kQ2hpbGQoYnRuX3NhdmUpO1xuICBsYXlvdXRfMS5hcHBlbmRDaGlsZChidG5fY2FuY2VsKTtcblxuICBpdGVtXzFfZWRpdC5hcHBlbmRDaGlsZChpY29uUGVuY2lsXzEpO1xuICBpdGVtXzJfZWRpdC5hcHBlbmRDaGlsZChpY29uUGVuY2lsXzIpO1xuICBpdGVtXzNfZWRpdC5hcHBlbmRDaGlsZChpY29uUGVuY2lsXzMpO1xuICBpdGVtXzRfZWRpdC5hcHBlbmRDaGlsZChpY29uUGVuY2lsXzQpO1xuXG4gIGl0ZW1fMV9jb250YWluZXIuYXBwZW5kQ2hpbGQoaXRlbV8xX3NwYW4pO1xuICBpdGVtXzEuYXBwZW5kQ2hpbGQoaXRlbV8xX2NvbnRhaW5lcik7XG4gIGl0ZW1fMS5hcHBlbmRDaGlsZChpdGVtXzFfZWRpdCk7XG5cbiAgaXRlbV8yX2NvbnRhaW5lci5hcHBlbmRDaGlsZChpdGVtXzJfc3Bhbik7XG4gIGl0ZW1fMi5hcHBlbmRDaGlsZChpY29uR2xvYmUpO1xuICBpdGVtXzIuYXBwZW5kQ2hpbGQoaXRlbV8yX2NvbnRhaW5lcik7XG4gIGl0ZW1fMi5hcHBlbmRDaGlsZChpdGVtXzJfZWRpdCk7XG5cbiAgaXRlbV8zX2NvbnRhaW5lci5hcHBlbmRDaGlsZChpdGVtXzNfc3Bhbik7XG4gIGl0ZW1fMy5hcHBlbmRDaGlsZChpY29uQ2FsbCk7XG4gIGl0ZW1fMy5hcHBlbmRDaGlsZChpdGVtXzNfY29udGFpbmVyKTtcbiAgaXRlbV8zLmFwcGVuZENoaWxkKGl0ZW1fM19lZGl0KTtcblxuICBpdGVtXzRfY29udGFpbmVyLmFwcGVuZENoaWxkKGl0ZW1fNF9zcGFuKTtcbiAgaXRlbV80LmFwcGVuZENoaWxkKGljb25Ib21lKTtcbiAgaXRlbV80LmFwcGVuZENoaWxkKGl0ZW1fNF9jb250YWluZXIpO1xuICBpdGVtXzQuYXBwZW5kQ2hpbGQoaXRlbV80X2VkaXQpO1xuXG4gIGxheW91dF8yLmFwcGVuZENoaWxkKGl0ZW1fMSk7XG4gIGxheW91dF8yLmFwcGVuZENoaWxkKGl0ZW1fMik7XG4gIGxheW91dF8yLmFwcGVuZENoaWxkKGl0ZW1fMyk7XG4gIGxheW91dF8yLmFwcGVuZENoaWxkKGl0ZW1fNCk7XG5cbiAgbWFpbi5hcHBlbmRDaGlsZChsYXlvdXRfMSk7XG4gIG1haW4uYXBwZW5kQ2hpbGQobGF5b3V0XzIpO1xuXG4gIC8vIGFwcGVuZCBuYXZcbiAgbWFpbi5hcHBlbmRDaGlsZChnZXRGb3JtQ29tcG9uZW50KCkpO1xuXG4gIHJldHVybiBtYWluO1xufVxuXG5mdW5jdGlvbiBjcmVhdGVFbGVtZW50KGVsVGFnLCBlbENsYXNzKSB7XG4gIGxldCBlbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoZWxUYWcpO1xuXG4gIGlmICh0eXBlb2YgZWxDbGFzcyA9PT0gJ3N0cmluZycpIHtcbiAgICBlbC5jbGFzc0xpc3QuYWRkKGVsQ2xhc3MpO1xuICB9IGVsc2UgaWYgKHR5cGVvZiBlbENsYXNzID09PSAnb2JqZWN0Jykge1xuICAgIGlmIChlbENsYXNzLmxlbmd0aCA+PSAxKSB7XG4gICAgICBlbENsYXNzLmZvckVhY2goYyA9PiB7XG4gICAgICAgIGVsLmNsYXNzTGlzdC5hZGQoYyk7XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gZWw7XG59XG5cbmV4cG9ydCB7IGdldENvbXBvbmVudCB9O1xuIiwiaW1wb3J0IHsgZ2V0Q29tcG9uZW50IGFzIGdldEhlYWRlckNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9oZWFkZXIvaW5kZXguanMnO1xuaW1wb3J0IHsgZ2V0Q29tcG9uZW50IGFzIGdldE1haW5Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvbWFpbi9pbmRleC5qcyc7XG5yZXF1aXJlKCcuLi9kaXN0L3N0eWxlLmNzcycpO1xuXG4vLyBjcmVhdGUgY29udGFpbmVyXG5sZXQgY29udGFpbmVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG5jb250YWluZXIuY2xhc3NMaXN0LmFkZCgnY29udGFpbmVyJyk7XG5cbi8vIGFwcGVuZCBjb250YWluZXJcbmRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoY29udGFpbmVyKTtcblxuLy8gYXBwZW5kIGhlYWRlclxuY29udGFpbmVyLmFwcGVuZENoaWxkKGdldEhlYWRlckNvbXBvbmVudCgpKTtcblxuLy8gYXBwZW5kIG1haW5cbmNvbnRhaW5lci5hcHBlbmRDaGlsZChnZXRNYWluQ29tcG9uZW50KCkpO1xuXG4vLyBhZnRlciBhcHBlbmQgbGlzdGVuZXJzXG5jb25zdCBzZXRBY3RpdmUgPSAoZWwsIGFjdGl2ZSkgPT4ge1xuICBjb25zdCBmb3JtRmllbGQgPSBlbC5wYXJlbnROb2RlO1xuICBpZiAoYWN0aXZlKSB7XG4gICAgZm9ybUZpZWxkLmNsYXNzTGlzdC5hZGQoJ2MtaW5wdXQtLWlzLWFjdGl2ZScpO1xuICB9IGVsc2Uge1xuICAgIGZvcm1GaWVsZC5jbGFzc0xpc3QucmVtb3ZlKCdjLWlucHV0LS1pcy1hY3RpdmUnKTtcbiAgICBlbC52YWx1ZSA9PT0gJydcbiAgICAgID8gZm9ybUZpZWxkLmNsYXNzTGlzdC5yZW1vdmUoJ2MtaW5wdXQtLWlzLWZpbGxlZCcpXG4gICAgICA6IGZvcm1GaWVsZC5jbGFzc0xpc3QuYWRkKCdjLWlucHV0LS1pcy1maWxsZWQnKTtcbiAgfVxufTtcblxuW10uZm9yRWFjaC5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5jLWlucHV0X19maWVsZCcpLCBlbCA9PiB7XG4gIGVsLm9uYmx1ciA9ICgpID0+IHtcbiAgICBzZXRBY3RpdmUoZWwsIGZhbHNlKTtcbiAgfTtcblxuICBlbC5vbmZvY3VzID0gKCkgPT4ge1xuICAgIHNldEFjdGl2ZShlbCwgdHJ1ZSk7XG4gIH07XG59KTtcblxuW10uZm9yRWFjaC5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5jLWlucHV0X19sYWJlbCcpLCBlbCA9PiB7XG4gIGVsLm9uY2xpY2sgPSAoKSA9PiB7XG4gICAgc2V0QWN0aXZlKGVsLCB0cnVlKTtcbiAgICBlbC5uZXh0U2libGluZy5mb2N1cygpO1xuICB9O1xufSk7XG5cbi8vIHNob3cgZWRpdFxuY29uc3QgYnRuX2UgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuYy1idG5fX2VkaXQnKTtcbmJ0bl9lLm9uY2xpY2sgPSAoKSA9PiB7XG4gIHNob3dFZGl0KGJ0bl9lKTtcbn07XG5cbmZ1bmN0aW9uIHNob3dFZGl0KGJ0bl9lKSB7XG4gIGxldCBkaXNwbGF5ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmMtYWx0ZXJfX2Rpc3BsYXknKTtcbiAgbGV0IGZvcm0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuYy1hbHRlcl9fZm9ybScpO1xuICBsZXQgYnRuX2MgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuYy1idG5fX2NhbmNlbCcpO1xuICBsZXQgYnRuX3MgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuYy1idG5fX3NhdmUnKTtcblxuICBkaXNwbGF5LnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XG4gIGZvcm0uc3R5bGUuZGlzcGxheSA9ICdibG9jayc7XG4gIGJ0bl9lLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XG4gIGJ0bl9jLnN0eWxlLmRpc3BsYXkgPSAnYmxvY2snO1xuICBidG5fcy5zdHlsZS5kaXNwbGF5ID0gJ2Jsb2NrJztcbn1cblxuZnVuY3Rpb24gaGlkZUVkaXQoKSB7XG4gIGxldCBkaXNwbGF5ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmMtYWx0ZXJfX2Rpc3BsYXknKTtcbiAgbGV0IGZvcm0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuYy1hbHRlcl9fZm9ybScpO1xuICBsZXQgYnRuX2MgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuYy1idG5fX2NhbmNlbCcpO1xuICBsZXQgYnRuX3MgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuYy1idG5fX3NhdmUnKTtcbiAgbGV0IGJ0bl9lID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmMtYnRuX19lZGl0Jyk7XG5cbiAgZGlzcGxheS5zdHlsZS5kaXNwbGF5ID0gJ2Jsb2NrJztcbiAgZm9ybS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xuICBidG5fYy5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xuICBidG5fcy5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xuICBidG5fZS5zdHlsZS5kaXNwbGF5ID0gJ2Jsb2NrJztcbn1cblxuLy8gaGlkZSBlZGl0XG5jb25zdCBidG5fYyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jLWJ0bl9fY2FuY2VsJyk7XG5idG5fYy5vbmNsaWNrID0gKCkgPT4ge1xuICBoaWRlRWRpdCgpO1xufTtcblxuY29uc3QgYnRuX3MgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuYy1idG5fX3NhdmUnKTtcbmJ0bl9zLm9uY2xpY2sgPSAoKSA9PiB7XG4gIGhpZGVFZGl0KCk7XG59O1xuXG4vLyBuYXYgbGlua3NcbltdLmZvckVhY2guY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuYy1saW5rJyksIGVsID0+IHtcbiAgZWwub25jbGljayA9ICgpID0+IHtcbiAgICBuYXZpZ2F0ZShlbCk7XG4gIH07XG59KTtcblxuZnVuY3Rpb24gbmF2aWdhdGUoZWwpIHtcbiAgbGV0IHRhYiA9IGVsLmlubmVySFRNTDtcbiAgbGV0IGFjdGl2ZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5hY3RpdmUnKTtcbiAgbGV0IHRpdGxlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmMtbGF5b3V0IGgzJyk7XG4gIGxldCBlZGl0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmMtYnRuX19lZGl0Jyk7XG4gIGxldCBjYW5jZWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuYy1idG5fX2NhbmNlbCcpO1xuICBsZXQgc2F2ZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jLWJ0bl9fc2F2ZScpO1xuICBsZXQgZGlzcGxheSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jLWFsdGVyX19kaXNwbGF5Jyk7XG4gIGxldCBmb3JtID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmMtYWx0ZXJfX2Zvcm0nKTtcblxuICBpZiAodGFiID09PSAnQWJvdXQnKSB7XG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoIDwgNzY4KSB7XG4gICAgICBlZGl0LnN0eWxlLmRpc3BsYXkgPSAnYmxvY2snO1xuICAgIH1cbiAgICBjb25zb2xlLmxvZyh3aW5kb3cuaW5uZXJXaWR0aCA8IDc2OCk7XG5cbiAgICBkaXNwbGF5LnN0eWxlLmRpc3BsYXkgPSAnYmxvY2snO1xuICB9IGVsc2Uge1xuICAgIGVkaXQuc3R5bGUuZGlzcGxheSA9ICdub25lJztcbiAgICBjYW5jZWwuc3R5bGUuZGlzcGxheSA9ICdub25lJztcbiAgICBzYXZlLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XG4gICAgZGlzcGxheS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xuICAgIGZvcm0uc3R5bGUuZGlzcGxheSA9ICdub25lJztcbiAgfVxuXG4gIHRpdGxlLmlubmVySFRNTCA9IHRhYjtcbiAgYWN0aXZlLmNsYXNzTGlzdC5yZW1vdmUoJ2FjdGl2ZScpO1xuICBlbC5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKTtcbn1cbiJdLCJzb3VyY2VSb290IjoiIn0=