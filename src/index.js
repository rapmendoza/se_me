import { getComponent as getHeaderComponent } from './components/header/index.js';
import { getComponent as getMainComponent } from './components/main/index.js';
require('../dist/style.css');

// create container
let container = document.createElement('div');
container.classList.add('container');

// append container
document.body.appendChild(container);

// append header
container.appendChild(getHeaderComponent());

// append main
container.appendChild(getMainComponent());

// after append listeners
const setActive = (el, active) => {
  const formField = el.parentNode;
  if (active) {
    formField.classList.add('c-input--is-active');
  } else {
    formField.classList.remove('c-input--is-active');
    el.value === ''
      ? formField.classList.remove('c-input--is-filled')
      : formField.classList.add('c-input--is-filled');
  }
};

[].forEach.call(document.querySelectorAll('.c-input__field'), el => {
  el.onblur = () => {
    setActive(el, false);
  };

  el.onfocus = () => {
    setActive(el, true);
  };
});

[].forEach.call(document.querySelectorAll('.c-input__label'), el => {
  el.onclick = () => {
    setActive(el, true);
    el.nextSibling.focus();
  };
});

// show edit
const btn_e = document.querySelector('.c-btn__edit');
btn_e.onclick = () => {
  showEdit(btn_e);
};

function showEdit(btn_e) {
  let display = document.querySelector('.c-alter__display');
  let form = document.querySelector('.c-alter__form');
  let btn_c = document.querySelector('.c-btn__cancel');
  let btn_s = document.querySelector('.c-btn__save');

  display.style.display = 'none';
  form.style.display = 'block';
  btn_e.style.display = 'none';
  btn_c.style.display = 'block';
  btn_s.style.display = 'block';
}

function hideEdit() {
  let display = document.querySelector('.c-alter__display');
  let form = document.querySelector('.c-alter__form');
  let btn_c = document.querySelector('.c-btn__cancel');
  let btn_s = document.querySelector('.c-btn__save');
  let btn_e = document.querySelector('.c-btn__edit');

  display.style.display = 'block';
  form.style.display = 'none';
  btn_c.style.display = 'none';
  btn_s.style.display = 'none';
  btn_e.style.display = 'block';
}

// hide edit
const btn_c = document.querySelector('.c-btn__cancel');
btn_c.onclick = () => {
  hideEdit();
};

const btn_s = document.querySelector('.c-btn__save');
btn_s.onclick = () => {
  hideEdit();
};

// nav links
[].forEach.call(document.querySelectorAll('.c-link'), el => {
  el.onclick = () => {
    navigate(el);
  };
});

function navigate(el) {
  let tab = el.innerHTML;
  let active = document.querySelector('.active');
  let title = document.querySelector('.c-layout h3');
  let edit = document.querySelector('.c-btn__edit');
  let cancel = document.querySelector('.c-btn__cancel');
  let save = document.querySelector('.c-btn__save');
  let display = document.querySelector('.c-alter__display');
  let form = document.querySelector('.c-alter__form');

  if (tab === 'About') {
    if (window.innerWidth < 768) {
      edit.style.display = 'block';
    }
    console.log(window.innerWidth < 768);

    display.style.display = 'block';
  } else {
    edit.style.display = 'none';
    cancel.style.display = 'none';
    save.style.display = 'none';
    display.style.display = 'none';
    form.style.display = 'none';
  }

  title.innerHTML = tab;
  active.classList.remove('active');
  el.classList.add('active');
}
