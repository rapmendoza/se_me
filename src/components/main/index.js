import globe from '../../assets/globe-outline.svg';
import call from '../../assets/call-outline.svg';
import home from '../../assets/home-outline.svg';
import pencil from '../../assets/pencil-outline.svg';
import { getComponent as getFormComponent } from './form/index.js';

function getComponent() {
  // create main
  let main = createElement('main', 'c-main');

  // create layouts
  let layout_1 = createElement('div', 'c-layout');
  let layout_2 = createElement('div', ['c-layout', 'c-alter__display']);

  // create h3
  let h3 = createElement('h3');
  h3.innerHTML = 'About';

  // create button
  let button = createElement('button', 'c-btn__edit');
  let btn_cancel = createElement('button', 'c-btn__cancel');
  btn_cancel.innerHTML = 'CANCEL';
  let btn_save = createElement('button', 'c-btn__save');
  btn_save.innerHTML = 'SAVE';

  // create items
  let item_1 = createElement('div', 'c-item');
  let item_2 = createElement('div', 'c-item');
  let item_3 = createElement('div', 'c-item');
  let item_4 = createElement('div', 'c-item');

  let item_1_span = createElement('span');
  let item_2_span = createElement('span');
  let item_3_span = createElement('span');
  let item_4_span = createElement('span');

  item_1_span.innerHTML = 'Jessica Parker';
  item_2_span.innerHTML = 'www.seller.com';
  item_3_span.innerHTML = '(949) 325 - 68594';
  item_4_span.innerHTML = 'Newport Beach, CA';

  let item_1_container = createElement('div', 'c-container');
  let item_2_container = createElement('div', 'c-container');
  let item_3_container = createElement('div', 'c-container');
  let item_4_container = createElement('div', 'c-container');

  let item_1_edit = createElement('div', 'c-edit');
  let item_2_edit = createElement('div', 'c-edit');
  let item_3_edit = createElement('div', 'c-edit');
  let item_4_edit = createElement('div', 'c-edit');

  // icons
  let iconGlobe = createElement('img', 'c-icon');
  iconGlobe.src = globe;

  let iconCall = createElement('img', 'c-icon');
  iconCall.src = call;

  let iconHome = createElement('img', 'c-icon');
  iconHome.src = home;

  let iconPencil_1 = createElement('img', 'c-icon');
  iconPencil_1.src = pencil;

  let iconPencil_2 = createElement('img', 'c-icon');
  iconPencil_2.src = pencil;

  let iconPencil_3 = createElement('img', 'c-icon');
  iconPencil_3.src = pencil;

  let iconPencil_4 = createElement('img', 'c-icon');
  iconPencil_4.src = pencil;

  let iconPencil_5 = createElement('img', 'c-icon');
  iconPencil_5.src = pencil;

  // APPENDS
  button.appendChild(iconPencil_5);

  layout_1.appendChild(h3);
  layout_1.appendChild(button);
  layout_1.appendChild(btn_save);
  layout_1.appendChild(btn_cancel);

  item_1_edit.appendChild(iconPencil_1);
  item_2_edit.appendChild(iconPencil_2);
  item_3_edit.appendChild(iconPencil_3);
  item_4_edit.appendChild(iconPencil_4);

  item_1_container.appendChild(item_1_span);
  item_1.appendChild(item_1_container);
  item_1.appendChild(item_1_edit);

  item_2_container.appendChild(item_2_span);
  item_2.appendChild(iconGlobe);
  item_2.appendChild(item_2_container);
  item_2.appendChild(item_2_edit);

  item_3_container.appendChild(item_3_span);
  item_3.appendChild(iconCall);
  item_3.appendChild(item_3_container);
  item_3.appendChild(item_3_edit);

  item_4_container.appendChild(item_4_span);
  item_4.appendChild(iconHome);
  item_4.appendChild(item_4_container);
  item_4.appendChild(item_4_edit);

  layout_2.appendChild(item_1);
  layout_2.appendChild(item_2);
  layout_2.appendChild(item_3);
  layout_2.appendChild(item_4);

  main.appendChild(layout_1);
  main.appendChild(layout_2);

  // append nav
  main.appendChild(getFormComponent());

  return main;
}

function createElement(elTag, elClass) {
  let el = document.createElement(elTag);

  if (typeof elClass === 'string') {
    el.classList.add(elClass);
  } else if (typeof elClass === 'object') {
    if (elClass.length >= 1) {
      elClass.forEach(c => {
        el.classList.add(c);
      });
    }
  }

  return el;
}

export { getComponent };
