import {
  getFirstName,
  getLastName,
  getLocation,
  getPhone,
  getWebsite
} from './group/index.js';

function getComponent() {
  let form = createElement('form', ['c-form', 'c-alter__form']);

  form.appendChild(getFirstName());
  form.appendChild(getLastName());
  form.appendChild(getLocation());
  form.appendChild(getPhone());
  form.appendChild(getWebsite());

  return form;
}

function createElement(elTag, elClass) {
  // create element
  let el = document.createElement(elTag);

  // adds class
  if (typeof elClass === 'string') {
    el.classList.add(elClass);
  } else if (typeof elClass === 'object') {
    if (elClass.length >= 1) {
      elClass.forEach(c => {
        el.classList.add(c);
      });
    }
  }

  return el;
}

export { getComponent };
