function getComponent(name, value) {
  let group = createElement('div', ['c-input__group', 'c-input--is-filled']);

  let label = createElement('label', 'c-input__label');
  label.setAttribute('for', name);
  label.innerHTML = labelize(name);

  let field = createElement('input', 'c-input__field');
  field.setAttribute('type', 'text');
  field.setAttribute('name', name);
  field.setAttribute('value', value);

  // APPEND
  group.appendChild(label);
  group.appendChild(field);

  return group;
}

function createElement(elTag, elClass) {
  // create element
  let el = document.createElement(elTag);

  // adds class
  if (typeof elClass === 'string') {
    el.classList.add(elClass);
  } else if (typeof elClass === 'object') {
    if (elClass.length >= 1) {
      elClass.forEach(c => {
        el.classList.add(c);
      });
    }
  }

  return el;
}

function getFirstName() {
  return getComponent('first_name', 'Jessica');
}

function getLastName() {
  return getComponent('last_name', 'Parker');
}

function getLocation() {
  return getComponent('location', 'Newport Beach, CA');
}

function getPhone() {
  return getComponent('phone', '(949) 325-68594');
}

function getWebsite() {
  return getComponent('website', 'www.seller.com');
}

function labelize(name) {
  return name.replace('_', ' ').toUpperCase();
}

export { getFirstName, getLastName, getLocation, getPhone, getWebsite };
