import imgsrc from './profile_image.jpg';
import star_o from '../../assets/star-outline.svg';
import star from '../../assets/star.svg';
import add from '../../assets/add-circle.svg';
import call from '../../assets/call-outline.svg';
import loc from '../../assets/location-outline.svg';

import { getComponent as getNavComponent } from './menu/index.js';

function getComponent() {
  // create header
  let header = createElement('header', 'c-header');

  // create layout_1
  let layout_1 = createElement('div', 'c-layout');

  // create layout_2
  let layout_2 = createElement('div', 'c-layout');

  // create layout_3
  let layout_3 = createElement('div', 'c-layout');

  let btn_wrapper = createElement('div', 'c-button__wrapper');

  //create button
  let button = createElement('button', ['c-button', 'c-button--primary']);
  button.innerHTML = 'Logout';

  // create 25% desktop
  let d_25 = createElement('div', 'c-container__25-d');
  let d_25_con = createElement('div', 'c-container');

  // create 75% desktop
  let d_75 = createElement('div', 'c-container__75-d');
  let d_75_con = createElement('div', 'c-container');

  // create image
  let image = createElement('img');
  image.src = imgsrc;

  // create infos
  let info_1 = createElement('div', 'c-info__list');
  info_1.innerHTML = 'Jessica Parker';

  let info_2 = createElement('div', 'c-info__list');
  let info_3 = createElement('div', 'c-info__list');

  let info_4 = createElement('div', 'c-info__list');

  let info_5 = createElement('div', 'c-info__list');
  info_5.innerHTML = '6 Reviews';

  // summon icons
  // star
  let icon_s_1 = createElement('img', 'c-icon');
  icon_s_1.src = star;
  let icon_s_2 = createElement('img', 'c-icon');
  icon_s_2.src = star;
  let icon_s_3 = createElement('img', 'c-icon');
  icon_s_3.src = star;
  let icon_s_4 = createElement('img', 'c-icon');
  icon_s_4.src = star;
  // star-outline
  let icon_so = createElement('img', 'c-icon');
  icon_so.src = star_o;
  // add-circle
  let icon_a = createElement('img', 'c-icon');
  icon_a.src = add;
  // location
  let icon_l = createElement('img', 'c-icon');
  icon_l.src = loc;
  // call
  let icon_p = createElement('img', 'c-icon');
  icon_p.src = call;

  let info_6 = createElement('div', 'c-info__list');

  // create divider
  let col__8 = createElement('div', 'c-col__7');
  let col__4 = createElement('div', 'c-col__5');

  // create hr
  let hr = createElement('hr');

  // APPENDS
  // append icons
  info_4.appendChild(icon_s_1);
  info_4.appendChild(icon_s_2);
  info_4.appendChild(icon_s_3);
  info_4.appendChild(icon_s_4);
  info_4.appendChild(icon_so);
  info_2.appendChild(icon_l);
  info_3.appendChild(icon_p);

  // append button
  btn_wrapper.appendChild(button);
  layout_1.appendChild(btn_wrapper);

  // append image
  d_25_con.appendChild(image);
  d_25.appendChild(d_25_con);

  // append info
  info_2.innerHTML += 'Newport Beach, CA';
  info_3.innerHTML += '(949) 325-68594';

  col__8.appendChild(info_1);
  col__8.appendChild(info_2);
  col__8.appendChild(info_3);
  d_75_con.appendChild(col__8);

  col__4.appendChild(info_4);
  col__4.appendChild(info_5);
  d_75_con.appendChild(col__4);

  // append 75 to container
  d_75.appendChild(d_75_con);

  // append followers to container
  info_6.appendChild(icon_a);
  info_6.innerHTML += '15 Followers';
  d_75.appendChild(info_6);

  // append nav
  d_75.appendChild(getNavComponent());

  // append containers
  layout_3.appendChild(d_25);
  layout_3.appendChild(d_75);

  // append layout-1
  header.appendChild(layout_1);

  // append layout-2
  header.appendChild(layout_2);

  // append hr
  header.appendChild(hr);

  // append layout-3
  header.appendChild(layout_3);

  return header;
}

function createElement(elTag, elClass) {
  let el = document.createElement(elTag);

  if (typeof elClass === 'string') {
    el.classList.add(elClass);
  } else if (typeof elClass === 'object') {
    if (elClass.length >= 1) {
      elClass.forEach(c => {
        el.classList.add(c);
      });
    }
  }

  return el;
}

export { getComponent };
