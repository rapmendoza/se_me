function getComponent() {
  // create nav
  let nav = createElement('nav', 'c-nav');

  // create ul
  let ul = createElement('ul', 'c-ul');

  // create list
  let li_1 = createElement('li', ['c-list', 'active']);
  let li_2 = createElement('li', 'c-list');
  let li_3 = createElement('li', 'c-list');
  let li_4 = createElement('li', 'c-list');
  let li_5 = createElement('li', 'c-list');

  // create link
  let a_1 = createElement('a', 'c-link');
  a_1.innerHTML = 'About';

  let a_2 = createElement('a', 'c-link');
  a_2.innerHTML = 'Settings';

  let a_3 = createElement('a', 'c-link');
  a_3.innerHTML = 'Option1';

  let a_4 = createElement('a', 'c-link');
  a_4.innerHTML = 'Option2';

  let a_5 = createElement('a', 'c-link');
  a_5.innerHTML = 'Option3';

  // APPENDS
  li_1.appendChild(a_1);
  li_2.appendChild(a_2);
  li_3.appendChild(a_3);
  li_4.appendChild(a_4);
  li_5.appendChild(a_5);

  ul.appendChild(li_1);
  ul.appendChild(li_2);
  ul.appendChild(li_3);
  ul.appendChild(li_4);
  ul.appendChild(li_5);

  nav.appendChild(ul);

  return nav;
}

function createElement(elTag, elClass) {
  // create element
  let el = document.createElement(elTag);

  // adds class
  if (typeof elClass === 'string') {
    el.classList.add(elClass);
  } else if (typeof elClass === 'object') {
    if (elClass.length >= 1) {
      elClass.forEach(c => {
        el.classList.add(c);
      });
    }
  }

  return el;
}

// const _getComponent = getComponent;
export { getComponent };
